package com.bnym.utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class SampleFileModifier {
	
	public static void createNACKRef(String filePathRef, String newFilePathRef) {
		String filePath = System.getProperty("user.dir") + "\\TestData\\Samples\\" + filePathRef;
		String newFilePath = System.getProperty("user.dir") + "\\TestData\\Samples\\" + newFilePathRef;
		
		System.out.println("Reference Path : "+ filePath);
		System.out.println("Destination Path : "+ newFilePath);
		
		File newFile = new File(newFilePath);
		FileWriter writer = null;
		try {
			String data = new String(Files.readAllBytes(Paths.get(filePath)));
			writer = new FileWriter(newFile);
			writer.write(data);
		}catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				// Closing the resources
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void updateNackFile(String newFilePathRef, HashMap<String, String> getVal) {
		String newFilePath = System.getProperty("user.dir") + "\\TestData\\Samples\\" + newFilePathRef;
		File newFile = new File(newFilePath);
		FileWriter writer = null;
		try {
			String data = new String(Files.readAllBytes(Paths.get(newFilePath)));
			for (Map.Entry<String, String> newVal : getVal.entrySet()) {
				data = data.replace(newVal.getKey(), newVal.getValue());
			}
			writer = new FileWriter(newFile);
			writer.write(data);
		}catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				// Closing the resources
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	// Method to update Transaction reference number 20 field for payment and statement
	// arguments - filepath - path of file to be modified , type - inbound/outbound/statement, id - 103/202/940
	public static void updateTransactionReference(String filePath, String type , String id)
	{
		File fileToBeModified = new File(filePath);
		String oldContent = "";
		BufferedReader reader = null;
		FileWriter writer = null;
		try {
			reader = new BufferedReader(new FileReader(fileToBeModified));
			String line = reader.readLine();
			long unixTime = System.currentTimeMillis();
			String unix=Long.toString(unixTime).substring(1);
			unixTime=Long.parseLong(unix);
			while (line != null) {
				if (line.startsWith(":20:")) {
					if (type.equals("inbound")) {
						line = ":20:I" + id + unixTime;
						oldContent = oldContent + line + System.lineSeparator();
					} else if (type.equals("outbound")) {
						line = ":20:O" + id + unixTime;
						oldContent = oldContent + line + System.lineSeparator();
					} else if (type.equals("statement")) {
						line = ":20:S" + id + unixTime;
						oldContent = oldContent + line + System.lineSeparator();
					}
					line = reader.readLine();
				} else {
					String latest = line;
					line = reader.readLine();
					if (line != null && latest != "") {
						oldContent = oldContent + latest + System.lineSeparator();
					} else {
						oldContent = oldContent + latest;
					}

				}

			}
			writer = new FileWriter(fileToBeModified);
			writer.write(oldContent);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				// Closing the resources
				reader.close();
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	// Update statement file with Payment ID
	// arguments - newString - Original payment reference / Output instruction id , type- inbound/outbound
	public static void updateStatementFile(String filePath, String newString, String type) {
		File fileToBeModified = new File(filePath);
		String oldContent = "";
		BufferedReader reader = null;
		FileWriter writer = null;
		try {
			reader = new BufferedReader(new FileReader(fileToBeModified));
			// Reading all the lines of input text file into oldContent
			String line = reader.readLine();
			while (line != null) {
				if (line.startsWith(":61:")) {
					if (type.equals("outbound")) {
						String[] data = line.split("00S103");
						line = data[0] + "00S103" + newString;
						oldContent = oldContent + line + System.lineSeparator();
					} else if(type.equals("nchg")) {
						String[] data = line.split("00NCHG");
						line = data[0] + "00NCHG" + newString;
						oldContent = oldContent + line + System.lineSeparator();
					} else if(type.equals("inbound")){
						String[] data = line.split("//");
						line = data[0] + "//" + newString;
						oldContent = oldContent + line + System.lineSeparator();
					} else {
						oldContent = oldContent + line + System.lineSeparator();
					}
					line = reader.readLine();
				}
				else if(line.startsWith("/MREF/")) {
					if(type.equals("mref")) {
						line = "/MREF/" + newString;
						oldContent = oldContent + line + System.lineSeparator();
					} else {
						oldContent = oldContent + line + System.lineSeparator();
					}
					line = reader.readLine();
				}
				else if(line.startsWith(":21:")) {
					if(type.equals("f21")) {
						line = ":21:" + newString;
						oldContent = oldContent + line + System.lineSeparator();
					}else {
						oldContent = oldContent + line + System.lineSeparator();
					}
					line = reader.readLine();
				}
				else {
					String latest = line;
					line = reader.readLine();
					if (line != null) {
						oldContent = oldContent + latest + System.lineSeparator();
					} else {
						oldContent = oldContent + latest;
					}
				}
			}
			writer = new FileWriter(fileToBeModified);
			writer.write(oldContent);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				// Closing the resources
				reader.close();
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	// Update statement file with Payment ID - used on TC33
	public static void modifyFileInboundOutbound(String filePath, String newString, String type) {
		File fileToBeModified = new File(filePath);
		String oldContent = "";
		BufferedReader reader = null;
		FileWriter writer = null;
		Boolean jobDone = false;
		Integer counter = 0;
		try {
			reader = new BufferedReader(new FileReader(fileToBeModified));
			String line = reader.readLine();
			while (line != null) {
				if (line.startsWith(":61:") && jobDone.equals(false)) {
					counter = counter + 1;
					if (type.equals("outbound") && counter.equals(1)) {
						String[] data = line.split("00S103");
						line = data[0] + "00S103" + newString;
						oldContent = oldContent + line + System.lineSeparator();
						jobDone = true;
					} else if (type.equals("inbound") && jobDone.equals(false) && counter.equals(2)) {
						String[] data = line.split("//");
						line = data[0] + "//" + newString;
						oldContent = oldContent + line + System.lineSeparator();
						jobDone = true;
					} else {
						oldContent = oldContent + line + System.lineSeparator();
					}
					line = reader.readLine();
				} else {
					String latest = line;
					line = reader.readLine();
					if (line != null && latest != "") {
						oldContent = oldContent + latest + System.lineSeparator();
					} else {
						oldContent = oldContent + latest;
					}

				}

			}
			writer = new FileWriter(fileToBeModified);
			writer.write(oldContent);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				// Closing the resources
				reader.close();
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void updateValueDate(String filePath, String type) {
		File fileToBeModified = new File(filePath);
		String oldContent = "";
		BufferedReader reader = null;
		FileWriter writer = null;
		try {
			reader = new BufferedReader(new FileReader(fileToBeModified));
			String line = reader.readLine();
			long unixTime = System.currentTimeMillis();
			String unix = Long.toString(unixTime).substring(1);
			unixTime = Long.parseLong(unix);
			DateFormat dateFormat = new SimpleDateFormat("yyMMdd");
			Date date = new Date();
			String dt = dateFormat.format(date);
			while (line != null) {
				if (line.startsWith(":32A:")) {
					if (type.equals("payment")) {
						line = ":32A:" + dt + line.substring(11);
						oldContent = oldContent + line + System.lineSeparator();
					}
					line = reader.readLine();
				} else if (line.startsWith(":60F:") && type.equals("statement")) {
					line = ":60F:" + line.charAt(5) + dt + line.substring(12);
					oldContent = oldContent + line + System.lineSeparator();
					line = reader.readLine();
				} else if (line.startsWith(":60M:") && type.equals("statement")) {
					line = ":60M:" + line.charAt(5) + dt + line.substring(12);
					oldContent = oldContent + line + System.lineSeparator();
					line = reader.readLine();					
				} else if (line.startsWith(":62F:") && type.equals("statement")) {
					line = ":62F:" + line.charAt(5) + dt + line.substring(12);
					oldContent = oldContent + line + System.lineSeparator();
					line = reader.readLine();
				} else if (line.startsWith(":62M:") && type.equals("statement")) {
					line = ":62M:" + line.charAt(5) + dt + line.substring(12);
					oldContent = oldContent + line + System.lineSeparator();
					line = reader.readLine();
				} else if (line.startsWith(":61:") && type.equals("statement")) {
					line = ":61:" + dt + line.substring(10);
					oldContent = oldContent + line + System.lineSeparator();
					line = reader.readLine();
				} else {
					String latest = line;
					line = reader.readLine();
					if (line != null && latest != "") {
						oldContent = oldContent + latest + System.lineSeparator();
					} else {
						oldContent = oldContent + latest;
					}

				}

			}
			writer = new FileWriter(fileToBeModified);
			writer.write(oldContent);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				// Closing the resources
				reader.close();
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	// Update AWH XML file with Message Reference ID - used on Inbound Reject cases
	public static void UpdateXML(String filePath, String value, String type) {
		File fileToBeModified = new File(filePath);
		String oldContent = "";
		BufferedReader reader = null;
		FileWriter writer = null;
		try {
			reader = new BufferedReader(new FileReader(fileToBeModified));
			String line = reader.readLine();
			while (line != null) {
				if (line.contains("<MessageReference>")) {
					String[] data = line.split("<MessageReference>");
					if (type.equals("MessageReference")) {
						line = data[0] + "<MessageReference>" + value + "</MessageReference>";
						oldContent = oldContent + line + System.lineSeparator();
					}
					line = reader.readLine();
				} else {
					String latest = line;
					line = reader.readLine();
					if (line != null && latest != "") {
						oldContent = oldContent + latest + System.lineSeparator();
					} else {
						oldContent = oldContent + latest;
					}

				}
			}
			writer = new FileWriter(fileToBeModified);
			writer.write(oldContent);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				// Closing the resources
				reader.close();
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	// Method to update 28 field for statement
	public static void updateStatementF28field(String filePath, String val, int i) {
		File fileToBeModified = new File(filePath);
		String oldContent = "";
		BufferedReader reader = null;
		FileWriter writer = null;
		try {
			reader = new BufferedReader(new FileReader(fileToBeModified));
			String line = reader.readLine();
			long unixTime = System.currentTimeMillis();
			String unix = Long.toString(unixTime).substring(1);
			unixTime = Long.parseLong(unix);
			while (line != null) {
				if (line.startsWith(":28C:")) {
					line = ":28C:" + "0" + val + "/00" + i;
					oldContent = oldContent + line + System.lineSeparator();
					line = reader.readLine();
				} else {
					String latest = line;
					line = reader.readLine();
					if (line != null && latest != "") {
						oldContent = oldContent + latest + System.lineSeparator();
					} else {
						oldContent = oldContent + latest;
					}

				}

			}
			writer = new FileWriter(fileToBeModified);
			writer.write(oldContent);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				// Closing the resources
				reader.close();
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	//update greater (tomorrow) date value 
	public static void updateValueDateGreater(String filePath, String type) {
		File fileToBeModified = new File(filePath);
		String oldContent = "";
		BufferedReader reader = null;
		FileWriter writer = null;
		try {
			reader = new BufferedReader(new FileReader(fileToBeModified));
			String line = reader.readLine();
			long unixTime = System.currentTimeMillis();
			String unix = Long.toString(unixTime).substring(1);
			unixTime = Long.parseLong(unix);
			DateFormat dateFormat = new SimpleDateFormat("yyMMdd");
			Date today = new Date();
			Date tomorrow = new Date(today. getTime() + (1000 * 60 * 60 * 24));			
			String dt = dateFormat.format(tomorrow);
			while (line != null) {
				if (line.startsWith(":32A:")) {
					if (type.equals("payment")) {
						line = ":32A:" + dt + line.substring(11);
						oldContent = oldContent + line + System.lineSeparator();
					}
					line = reader.readLine();							
				} else if (line.startsWith(":62F:") && type.equals("statement")) {
					line = ":62F:" + line.charAt(5) + dt + line.substring(12);
					oldContent = oldContent + line + System.lineSeparator();
					line = reader.readLine();				
				} else {
					String latest = line;
					line = reader.readLine();
					if (line != null && latest != "") {
						oldContent = oldContent + latest + System.lineSeparator();
					} else {
						oldContent = oldContent + latest;
					}

				}

			}
			writer = new FileWriter(fileToBeModified);
			writer.write(oldContent);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				// Closing the resources
				reader.close();
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	//update lesser (yesterday) date value 
	public static void updateValueDateLesser(String filePath, String type) {
		File fileToBeModified = new File(filePath);
		String oldContent = "";
		BufferedReader reader = null;
		FileWriter writer = null;
		try {
			reader = new BufferedReader(new FileReader(fileToBeModified));
			String line = reader.readLine();
			long unixTime = System.currentTimeMillis();
			String unix = Long.toString(unixTime).substring(1);
			unixTime = Long.parseLong(unix);
			DateFormat dateFormat = new SimpleDateFormat("yyMMdd");
			Date today = new Date();
			Date tomorrow = new Date(today. getTime() - (1000 * 60 * 60 * 24));			
			String dt = dateFormat.format(tomorrow);
			while (line != null) {
				if (line.startsWith(":32A:")) {
					if (type.equals("payment")) {
						line = ":32A:" + dt + line.substring(11);
						oldContent = oldContent + line + System.lineSeparator();
					}
					line = reader.readLine();							
				} else if (line.startsWith(":62F:") && type.equals("statement")) {
					line = ":62F:" + line.charAt(5) + dt + line.substring(12);
					oldContent = oldContent + line + System.lineSeparator();
					line = reader.readLine();				
				} else {
					String latest = line;
					line = reader.readLine();
					if (line != null && latest != "") {
						oldContent = oldContent + latest + System.lineSeparator();
					} else {
						oldContent = oldContent + latest;
					}

				}

			}
			writer = new FileWriter(fileToBeModified);
			writer.write(oldContent);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				// Closing the resources
				reader.close();
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	
	// get field 72 data from sample file
    public static String[] getFieldData(String filePath , String type) {
    	String data = "";
    	String [] splitData;
    	try {
    		if(type.equals("MT900")) {
    			data = new String(Files.readAllBytes(Paths.get(filePath)));
        		splitData = data.split(":72:");
        		splitData = splitData[1].split("-}");
        		splitData[0] = splitData[0].replace("/", "");
        		splitData[0] = splitData[0].replace("\r", "");
        		splitData = splitData[0].split("\n");
        	    return splitData; 
    		}else if(type.equals("MT910")) {
    			data = new String(Files.readAllBytes(Paths.get(filePath)));
        		splitData = data.split(":72:");
        		splitData = splitData[1].split("-}");
        		splitData[0] = splitData[0].replace("/", "");
        		splitData[0] = splitData[0].replace("\r", "");
        		splitData[0] = splitData[0].replace(" ", "");
        		splitData = splitData[0].split("\n");
        	    return splitData; 
    		}else {
    			return new String[1];
    		}	
    	}catch(Exception e) {
    		return new String[1];
    	}
	}
	
		// get ACCP and SUCCESS from the download file for Force Accept 
		public static String forceAcceptComp(String filePath, String getStatus) throws Exception {
		 		
			String statusVal = null;
			FileInputStream fileObj=new FileInputStream(filePath);       
			 Scanner scan=new Scanner(fileObj);    	// Open scanner to read file  
			 while(scan.hasNextLine())  			  	// Check if next line there to read
			 {  
				 String line = scan.nextLine();	  	// Scan & store corresponding line
				 if (line.contains(getStatus)) {
					 Pattern pattern = Pattern.compile(".*\\<" + getStatus + "> *(.*) *\\</" + getStatus + ">.*");	// Create pattern using "getStatus"
					  Matcher matcher = pattern.matcher(line);	// Create matcher by using pattern
					 matcher.find();								// Find matching values 
					 statusVal = matcher.group(1);				// Store it to variable
		// 		        System.out.println(statusVal);
				 }
			 }  
			 scan.close();     //close the scanner
			return statusVal;
		 }
		   
		//get essential field data from sample file for Force Accept and LineTransfer
		
		public static String getFieldValue(String fileName, String fieldVal) throws Exception{
			String data = "";
		 	String [] splitData;
				try {
					if(fieldVal.equals(":20:") || fieldVal.equals(":21:") || fieldVal.equals(":57A:") ) {
						data = new String(Files.readAllBytes(Paths.get(fileName)));
						splitData = data.split(fieldVal);
						splitData = splitData[1].split("\n");
						return splitData[0];
					}else if(fieldVal.equals(":32A:")){
						data = new String(Files.readAllBytes(Paths.get(fileName)));
						splitData = data.split(fieldVal);
						splitData = splitData[1].split("\n");
						splitData[0] = splitData[0].replace(",", "");
						return splitData[0];
					}else if(fieldVal.equals(":25P:") || fieldVal.equals(":52A:") || fieldVal.equals(":58D:")){
						data = new String(Files.readAllBytes(Paths.get(fileName)));			
						splitData = data.split(fieldVal);
						splitData = splitData[1].split(":");
						return splitData[0];
					}else if(fieldVal.equals(":50A:") || fieldVal.equals(":59A:")) {
						data = new String(Files.readAllBytes(Paths.get(fileName)));	
						splitData = data.split(fieldVal);
						splitData[0] = splitData[0].replace("/", "");
						splitData[0] = splitData[0].replace("\r", "");
						splitData = splitData[0].split("\n");
						return splitData[0];
					}else if(fieldVal.equals(":53B:")) {
						data = new String(Files.readAllBytes(Paths.get(fileName)));
						splitData = data.split(fieldVal);
						
						splitData[0] = splitData[0].replace("/", "");
						
						splitData = splitData[1].split("\n");
						return splitData[0];
					}else if(fieldVal.equals(":72:")) {
						data = new String(Files.readAllBytes(Paths.get(fileName)));
						splitData = data.split(fieldVal);
						
						splitData[0] = splitData[0].replace("/", "");
						
						splitData = splitData[1].split("\n");
						return splitData[0];
					}
					else {
						return null;
					}
				}
				catch (Exception et) {
					System.out.println(et.getMessage());
					return et.getMessage();
				}
		}
		
		public static String getFieldValueFunding(String fileName, String fieldVal) throws Exception{
				String data = "";
				String [] splitData;
				try {
					if(fieldVal.equals(":32A:")){
						data = new String(Files.readAllBytes(Paths.get(fileName)));
						splitData = data.split(fieldVal);
						splitData = splitData[1].split("\n");
						splitData[0] = splitData[0].replace(",", "");
						return splitData[0];
					}else if(fieldVal.equals(":57A:") || fieldVal.equals(":52A:") || fieldVal.equals(":58A:")){
						data = new String(Files.readAllBytes(Paths.get(fileName)));			
						splitData = data.split(fieldVal);
						splitData = splitData[1].split(":");
						return splitData[0];
					}else if(fieldVal.equals(":53B:")) {
						data = new String(Files.readAllBytes(Paths.get(fileName)));
						splitData = data.split(fieldVal);
						
						splitData[0] = splitData[0].replace("/", "");
						
						splitData = splitData[1].split("\n");
						return splitData[0];
					}else if(fieldVal.equals(":72:")){
						data = new String(Files.readAllBytes(Paths.get(fileName)));			
						splitData = data.split(fieldVal);
						splitData[1] = splitData[1].replace("-}"," ");
						splitData = splitData[1].split(":");
						return splitData[0];
					}else {
						return null;
					}
				}
				catch (Exception et) {
					System.out.println(et.getMessage());
					return et.getMessage();
				}
		}
		
		//get FieldVal from sample File
	    public static String getSampleFileFieldValue(String filePath, String fieldVal) throws Exception{
			String data = "";
	    	String [] splitData;
			try {
				if(fieldVal.equals(":72:")) {
					data = new String(Files.readAllBytes(Paths.get(filePath)));
	        		splitData = data.split(":72:");
	        		splitData = splitData[1].split("-}");
	        		splitData[0] = splitData[0].replace("/", "");
	        		splitData[0] = splitData[0].replace("\r", "");
	        		splitData[0] = splitData[0].replace(" ", "");
	        		splitData = splitData[0].split("\n");
	        		String retData = String.join("", splitData);
	        		return retData;
				}
				else {
					data = new String(Files.readAllBytes(Paths.get(filePath)));
					splitData = data.split(fieldVal);
	        		splitData = splitData[1].split(":");
	        		splitData[0] = splitData[0].replace("/", "");
	        		splitData[0] = splitData[0].replace("\r", "");
	        		splitData[0] = splitData[0].replace(" ", "");
	        		splitData = splitData[0].split("\n");
	        		String retData = String.join("", splitData);
	        		return retData;
				}
			}
			catch (Exception et) {
				System.out.println(et.getMessage());
				return et.getMessage();
			}
		}
	
 	
// End of Class    
}
