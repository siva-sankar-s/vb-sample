package com.bnym.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.bnym.utilities.Constants;
import com.bnym.utilities.WaitLibrary;
import com.paulhammant.ngwebdriver.ByAngular;
import com.paulhammant.ngwebdriver.ByAngularRepeater;

public class ManualMatching {
	
	public static By type = ByAngular.repeater("type in Types");
	public static By match = ByAngular.repeater("match in Matches");
	public static By with = ByAngular.repeater("with in dataWith");
	public static By matchPayments = ByAngular.repeater("data in sectionTwoDatas");

		// Locating Matching Module
		public static WebElement matchingModule(WebDriver driver) {
			By matchingModule = By.xpath("//span[contains(text(),'Matching Module')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, matchingModule);
			return driver.findElement(matchingModule);
		}
		//Locate Manual Match from side bar
		public static WebElement manualMatching(WebDriver driver) {
			By manualMatching = By.xpath("//span[@class='ng-binding'][contains(text(),'Manual Matching')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, manualMatching);
			return driver.findElement(manualMatching);
		}
		//Get matching type select box
		public static WebElement matchingType(WebDriver driver) {
			By matchingType = By.xpath("//select[contains(@ng-change,'selectType(matching.Types)')][@id='Types']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, matchingType);
			return driver.findElement(matchingType);
		}
		
		//Get matching type Options 
		public static WebElement matchingTypeOptions(WebDriver driver) {
			By matchingTypeOptions = By.xpath("//option[@ng-repeat='type in Types'][contains(@value,'paymentstatement')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, matchingTypeOptions);
			return driver.findElement(matchingTypeOptions);
		}
		
		//Get match select box
		public static WebElement matchSelect(WebDriver driver) {
			By match = By.xpath("//select[contains(@ng-change,'selectmatch(matching.Match,matching.Types)')][@id='Match']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, match);
			return driver.findElement(match);
		}
		
		// Get match options
		public static WebElement matchOptions(WebDriver driver) {
			By matchOptions = By.xpath("//option[@ng-repeat='match in Matches'][contains(@value,'payment')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, matchOptions);
			return driver.findElement(matchOptions);
		}
		
		//Get with select box
		public static WebElement withSelect(WebDriver driver) {
			By with = By.xpath("//select[contains(@ng-change,'selectwith(matching.With)')][@id='With']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, with);
			return driver.findElement(with);
		}
		
		// Get with options
		public static WebElement withOptions(WebDriver driver) {
			By withOptions = By.xpath("//option[@ng-repeat='with in dataWith'][contains(@value,'bankstatement')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, withOptions);
			return driver.findElement(withOptions);
		}
		
		// Get Match Search Box
		public static WebElement matchSearchBox(WebDriver driver) {
			By matchSearchBoxElem = By.xpath("//div[@id='Match']//input[@id='searchBox']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, matchSearchBoxElem);
			return driver.findElement(matchSearchBoxElem);
		}
		
		// Get With Search Box
		public static WebElement withSearchBox(WebDriver driver) {
			By withSearchBoxElem = By.xpath("//div[@id='With']//input[@id='searchBox']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, withSearchBoxElem);
			return driver.findElement(withSearchBoxElem);
		}
		
		// Get Match Div Search Icon
		public static WebElement matchSearchBtn(WebDriver driver) {
			By matchSearchBtn = By.xpath("//div[@id='Match']//span[contains(@ng-click,'uorSearch($event)')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, matchSearchBtn);
			return driver.findElement(matchSearchBtn);
		}
		
		// Get With Div Search Icon
		public static WebElement withSearchBtn(WebDriver driver) {
			By withSearchBtn = By.xpath("//div[@id='With']//span[contains(@ng-click,'uorSearch($event)')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, withSearchBtn);
			return driver.findElement(withSearchBtn);
		}
		
		// click anywhere on the payments
		public static WebElement clickPmts(WebDriver driver, int row, String col) {
			By clickPmts = ((ByAngularRepeater) matchPayments).row(row).column(col);
			return driver.findElement(clickPmts);
		}
		
		// click match checkbox
		public static WebElement matchCheckBox(WebDriver driver) {
			By matchCheckBox = By.xpath("//tr[contains(@ng-repeat,'data in sectionTwoDatas')]//td[@class='txtCenter']//input[@type='checkbox']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, matchCheckBox);
			return driver.findElement(matchCheckBox);
		}
		
		// click with checkbox
		public static WebElement withCheckBox(WebDriver driver) {
			By withCheckBox = By.xpath("//tr[contains(@ng-repeat,'data in sectionThreeDatas')]//td[@class='txtCenter']//input[@type='checkbox']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, withCheckBox);
			return driver.findElement(withCheckBox);
		}
		
		//click on force match
		public static WebElement forceMatch(WebDriver driver) {
			By forceMatch = By.xpath("//button[@type='button'][contains(.,'FORCE MATCH')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, forceMatch);
			return driver.findElement(forceMatch);
		}
		
		//click on Submit
		public static WebElement submitMatchingType(WebDriver driver) {
			By submitMatchingType = By.xpath("//button[@class='btn btnStyle'][contains(text(),'Submit')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, submitMatchingType);
			return driver.findElement(submitMatchingType);
		}
		
		// Get Statement generate balance adjustment select box
		public static WebElement balanceAdjust(WebDriver driver) {
			By balAdjustElem = By.xpath("//select[contains(@name1,'ShouldSystemGenerateBalanceAdjustment')]/following-sibling::span");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, balAdjustElem);
			return driver.findElement(balAdjustElem);
		}
		
		// Get Statement adjustment account numeber select box
		public static WebElement adjustAccNo(WebDriver driver) {
			By adjustAccNoElem = By.xpath("//select[contains(@name1,'AccountNo')]/following-sibling::span");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, adjustAccNoElem);
			return driver.findElement(adjustAccNoElem);
		}
		
		//click submit with account
		public static WebElement accountConfirm(WebDriver driver) {
			By accountConfirm = By.xpath("//span[@class='ng-scope'][contains(.,'Submit')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, accountConfirm);
			return driver.findElement(accountConfirm);
		}
		
	}