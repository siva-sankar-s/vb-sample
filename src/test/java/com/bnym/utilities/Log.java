package com.bnym.utilities;

import java.util.Date;
import java.util.logging.LogRecord;

import org.apache.log4j.Logger;

import com.relevantcodes.extentreports.LogStatus;

public class Log {
	public static Logger log;

	//public static final int LOGTYPE_TIME_PASS_FAIL = 1;

	//public static int giAutomationLogType = LOGTYPE_TIME_PASS_FAIL;

	// private static Logger Log = Logger.getLogger(Log.class.getName());//

	// This is to print log for the beginning of the test case, as we usually run so
	// many test cases as a test suite

	public static void startTestCase(Logger log, String sTestCaseName) {

		log.info("****************************************************************************************");

		log.info("****************************************************************************************");

		log.info("$$$$$$$$$$$$$$$$$$$$$                 " + sTestCaseName + "       $$$$$$$$$$$$$$$$$$$$$$$$$");

		log.info("****************************************************************************************");

		log.info("****************************************************************************************");

	}

	// This is to print log for the ending of the test case

	public static void endTestCase(Logger log, String sTestCaseName) {

		log.info("XXXXXXXXXXXXXXXXXXXXXXX             " + "-E---N---D-" + "             XXXXXXXXXXXXXXXXXXXXXX");

		log.info("X");

		log.info("X");

		log.info("X");

		log.info("X");

	}

	/*
	 * public static void info(String message) {
	 * 
	 * Log.info(message);
	 * 
	 * }
	 */

	public String logScriptInfo(LogRecord record) {
		return record.getThreadID() + "::" + record.getSourceClassName() + "::" + record.getSourceMethodName() + "::"
				+ new Date(record.getMillis()) + "::" + record.getMessage() + "\n";
	}

	public static void assertVerify(boolean bExpected, boolean bActual, String sDesc) throws Exception {
		if (bActual == bExpected) {
			System.out.println("Verify " + sDesc + " Expected:" + bExpected + " Actual:" + bActual);
		} else {
			throw new Exception("Error Verifying " + sDesc + " Expected:" + bExpected + " Actual:" + bActual);
		}
	}





	public static void asserVerify(boolean bExpected, boolean bActual, String sDesc) throws Exception {
		if (bActual == bExpected) {
			CaptureScreenshot.captureScreenshot();
			LoginLogout.test.log(LogStatus.PASS,"Verify " + sDesc + " Expected:" + bExpected + " Actual:" + bActual+"<br>"+LoginLogout.test.addScreenCapture(CaptureScreenshot.screenshotName));

		} else {
			CaptureScreenshot.captureScreenshot();
			LoginLogout.test.log(LogStatus.FAIL,"Verify " + sDesc + " Expected:" + bExpected + " Actual:" + bActual+"<br>"+LoginLogout.test.addScreenCapture(CaptureScreenshot.screenshotName));

			throw new Exception("Error Verifying " + sDesc + " Expected:" + bExpected + " Actual:" + bActual);
		}
	} 

}
