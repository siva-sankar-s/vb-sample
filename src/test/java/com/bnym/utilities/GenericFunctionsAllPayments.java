package com.bnym.utilities;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.bnym.pages.AllPaymentsPage;


public class GenericFunctionsAllPayments {
	
	public static JavascriptExecutor js ;
	
	
	public static void downloadOUTPUT(WebDriver driver) throws Exception
	{
		js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", AllPaymentsPage.goToExtComm(driver));
		js.executeScript("arguments[0].click();",AllPaymentsPage.downloadOutputFile(driver));
		Thread.sleep(3000);
		
	}
	
	public static void downloadRFP_Ack(WebDriver driver) throws Exception
	{
		js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", AllPaymentsPage.goToExtComm(driver));
		js.executeScript("arguments[0].click();",AllPaymentsPage.downloadRFPACK(driver));
		Thread.sleep(3000);
		
	}
	
	public static void downloadRRFP(WebDriver driver) throws Exception
	{
		js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", AllPaymentsPage.goToExtComm(driver));
		js.executeScript("arguments[0].click();",AllPaymentsPage.downloadRRFPOutput(driver));
		Thread.sleep(3000);
		
	}
	
	
	public static void downloadISOCAMT54(WebDriver driver) throws Exception
	{
		js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", AllPaymentsPage.goToExtComm(driver));
		Thread.sleep(1000);
		js.executeScript("arguments[0].click();",AllPaymentsPage.downloadISOCAMT054(driver));
		Thread.sleep(3000);
		
	}
	
	public static void downloadROFOUT(WebDriver driver) throws Exception
	{
		js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", AllPaymentsPage.goToExtComm(driver));
		Thread.sleep(1000);
		js.executeScript("arguments[0].click();",AllPaymentsPage.downloadROFOutput(driver));
		Thread.sleep(3000);
		
	}
	
	public static void downloadRROFOUTPUTS(WebDriver driver) throws Exception
	{
		js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", AllPaymentsPage.goToExtComm(driver));
		Thread.sleep(1000);
		js.executeScript("arguments[0].click();",AllPaymentsPage.downloadRROFOutput(driver));
		Thread.sleep(3000);
		
	}
	
	public static void downloadPmtInsOUTPUTLinkMsgs(WebDriver driver) throws Exception
	{
		js = (JavascriptExecutor)driver;
		//js.executeScript("arguments[0].click();", allPaymentsPage.goToExtComm(driver));
		Thread.sleep(1000);
		js.executeScript("arguments[0].click();",AllPaymentsPage.downloadOutputFile(driver));
		Thread.sleep(3000);
		
	}
	
	
	public static void downloadCAMT54LinkMsgs(WebDriver driver) throws Exception
	{
		js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", AllPaymentsPage.goToExtComm(driver));
		Thread.sleep(1000);
		js.executeScript("arguments[0].click();",AllPaymentsPage.downloadISOCAMT054(driver));
		Thread.sleep(3000);
		
	}
	
	
	public static void getAllBECNotifications(WebDriver driver,String Screenshots,String BECNOTIFICATIONS) throws Exception
	{

		List<WebElement> list;
		js = (JavascriptExecutor)driver;
		File dest;
		FileWriter fw;
		BufferedWriter out;
		Actions act;
		
		
		
		js.executeScript("arguments[0].click();", AllPaymentsPage.goToSysInteration(driver));
		Thread.sleep(1500);
		BrowserResolution.scrollDown(driver);
		Thread.sleep(1000);

		list = AllPaymentsPage.getAllBECS(driver);
		System.out.println("List of BECNOTIFICATIONS Elements::::"+list.size());

		for(int i=0; i<list.size(); i++)
		{
			System.out.println(i);
			Thread.sleep(2000);
			//js.executeScript("arguments[0].click();", list.get(i));
			list.get(i).click();
			Thread.sleep(2000);
			String BECTitle=AllPaymentsPage.getBECTitle(driver, i).getText();
			System.out.println("BECTitle is : "+BECTitle);

			dest=new File(BECNOTIFICATIONS+"\\"+BECTitle+"-"+i+".txt");
			fw = new FileWriter(dest);
			out = new BufferedWriter(fw);
			//System.out.println("XML_Content is :"+AllPaymentsPage.getBECContent(driver).getText());
			out.write(AllPaymentsPage.getBECContent(driver, i).getText());
			out.flush();
			out.close();
			Thread.sleep(2000);
			//js.executeScript("arguments[0].click();", allPaymentsPage.viewClose(driver));
            act = new Actions(driver);
			act.sendKeys(Keys.ESCAPE).build().perform();
		
			//AllPaymentsPage.viewClose(driver, i).click();
			Thread.sleep(3000);
			
			/*act = new Actions(driver);
			
			act.sendKeys(Keys.ESCAPE);*/




		}


		BrowserResolution.zoomOut(driver);
		Thread.sleep(1500);
		CaptureScreenshot.getScreenshotInPath(driver, Screenshots, "System_Interaction");
		BrowserResolution.defaultResol(driver);
		/*act = new Actions(driver);
		
		act.sendKeys(Keys.ESCAPE);*/
		Thread.sleep(2000);
		
		//allPaymentsPage.goToAllPayments(driver).click();
	}
	
	
	
	public static void getAllNotifications(WebDriver driver,String pmtId,String Screenshots,String NOTIFICATIONS) throws Exception
	{

		List<WebElement> list;
		js = (JavascriptExecutor)driver;
		File dest;
		FileWriter fw;
		BufferedWriter out;
	//	Actions act;
		String NotificationTitle;
		
		
		AllPaymentsPage.goToAllPayments(driver).click();
		Thread.sleep(1000);
		AllPaymentsPage.selectPaymentId(driver, pmtId).click();
		Thread.sleep(1500);
		
		js.executeScript("arguments[0].click();", AllPaymentsPage.goToSysInteration(driver));
		Thread.sleep(1500);
		BrowserResolution.scrollDown(driver);
		Thread.sleep(1000);

		list = AllPaymentsPage.getAllNotifications(driver);
		System.out.println("List of NOTIFICATIONS Elements::::"+list.size());

		for(int i=0; i<list.size(); i++)
		{
			//System.out.println(i);
			Thread.sleep(2500);
			//js.executeScript("arguments[0].click();", list.get(i));
			list.get(i).click();
			Thread.sleep(2000);
			NotificationTitle=AllPaymentsPage.getBECTitleByRowNum(driver,i).getText();
			if(NotificationTitle.contains(":"))
			{
				NotificationTitle=NotificationTitle.replace(":","_");
				
			}
			
			//System.out.println("Modified title is : "+BECTitle);

			dest=new File(NOTIFICATIONS+"\\"+NotificationTitle+"-"+i+".txt");
			fw = new FileWriter(dest);
			out = new BufferedWriter(fw);
			String content=AllPaymentsPage.getBECContentByRowNum(driver, i).getText();
			Thread.sleep(1500);
			//System.out.println("XML_Content is :"+allPaymentsPage.getBECContentByRowNum(driver,i).getText());
			out.write(content);
			out.flush();
			out.close();
			
			//js.executeScript("arguments[0].click();", allPaymentsPage.viewClose(driver));
			AllPaymentsPage.viewCloseByRowNum(driver,i).click();
			Thread.sleep(4000);
			
			/*act = new Actions(driver);
			
			act.sendKeys(Keys.ESCAPE);*/




		}


		BrowserResolution.zoomOut(driver);
		Thread.sleep(1500);
		CaptureScreenshot.getScreenshotInPath(driver, Screenshots, "System_Interaction");
		BrowserResolution.defaultResol(driver);
		/*act = new Actions(driver);
		act.sendKeys(Keys.ESCAPE);*/
		Thread.sleep(2000);
		
		//allPaymentsPage.goToAllPayments(driver).click();
	}
	
	public static void gotoAllPayments(WebDriver driver)
	{
		try{
		js.executeScript("arguments[0].click();", AllPaymentsPage.homeModule(driver));
		Thread.sleep(3000);
		js.executeScript("arguments[0].click();", AllPaymentsPage.paymentModule(driver));
		Thread.sleep(2000);
		}
		catch(InterruptedException ie)
		{
			ie.printStackTrace();
		}
		
	}
	
	public static void getAllDownloadsSysIntTab(WebDriver driver, String NOTIFICATIONS) throws Exception
	{

		BrowserResolution.zoomOut(driver);
		Thread.sleep(1000);
		List<WebElement> list=null;
		list=AllPaymentsPage.getAllDownloadFiles(driver);
	    System.out.println("list:"+list.size());
		for(int i=1;i<=list.size();i++){
			System.out.println("Download the all output files");
			driver.findElement(By.xpath("//tr["+i+"]//td[8]//span[1]")).click();
			
		//	js.executeScript("arguments[0].click();",driver.findElement(By.xpath("//tr["+i+"]//td[8]//span[1]")));
			Thread.sleep(4000);
			
			String direction=driver.findElement(By.xpath("//tr["+i+"]//td[8]//span[1]/preceding::td[3]")).getText();
			Thread.sleep(4000);
			System.out.println("direction:"+direction);
			String msgid=driver.findElement(By.xpath("//tr["+i+"]//td[8]//span[1]/preceding::td[6]")).getText();
			Thread.sleep(4000);
			System.out.println("msgid:"+msgid);
			//String finalfile=direction+msgid+".txt";
			RenameFiles.renameFileName(Constants.DownloadsPath,NOTIFICATIONS,"finalfile"+i+".txt");
			Thread.sleep(4000);
			direction=null;
			msgid=null;
		
		}	
	}

}
	
