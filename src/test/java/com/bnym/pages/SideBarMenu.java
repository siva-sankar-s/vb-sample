package com.bnym.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.bnym.utilities.Constants;
import com.bnym.utilities.WaitLibrary;

public class SideBarMenu {

	public static WebElement element = null;

	// Home menu	
	public static WebElement homeModule(WebDriver driver) {
		By homeMenu = By.xpath("//span[contains(text(),'Home')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, homeMenu);
//		element = driver.findElement(By.xpath("//span[contains(text(),'Home')]"));
		return driver.findElement(homeMenu);
	}
	
	// Payment Module
	public static WebElement paymentModule(WebDriver driver) {
		By paymentModule = By.xpath("//a[contains(@title,'Payment Module')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, paymentModule);
		return driver.findElement(paymentModule);
	}
	
	// Payment Module
	public static WebElement allPayments(WebDriver driver) {
		By allPayments = By.xpath("//a[contains(@title,'All Payments')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, allPayments);
		return driver.findElement(allPayments);
	}
		
	// Received Instructions
	public static WebElement recInsTab(WebDriver driver) {
	By recInsTab = By.xpath("//a[contains(@title,'Received Instructions')]");
	WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, recInsTab);
	return driver.findElement(recInsTab);
	}
	
	// Locating My Profile Module
	public static WebElement myProfile(WebDriver driver) {
		By myProf = By.xpath("//span[contains(text(),'My Profile')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, myProf);
		return driver.findElement(myProf);
	}
	
	// Onboarding Data
	public static WebElement onboardingData(WebDriver driver) {
		By OnboardingData = By.xpath("//span[contains(text(),'Onboarding Data')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, OnboardingData);
		return driver.findElement(OnboardingData);
	}
	
	// Transport module
	public static WebElement transportTab(WebDriver driver) {
		By transportTab = By.xpath("//span[contains(text(),'Transport')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, transportTab);
		return driver.findElement(transportTab);
	}
	
	// Add On 
	public static WebElement addOn(WebDriver driver) {
		By AddOn = By.xpath("//span[contains(text(),'Add on')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, AddOn);
		return driver.findElement(AddOn);
	}
	
	// Amount limit profile
	public static WebElement amtLimitProfile(WebDriver driver) {
		By AmtLimitProfile = By.xpath("//span[contains(text(),'Amount Limit Profile')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, AmtLimitProfile);
		return driver.findElement(AmtLimitProfile);
	}
	
	// Party Service Asssociation
	public static WebElement partyServiceAss(WebDriver driver) {
		By partyServiceAss = By.xpath("//span[contains(text(),'Party Service Association')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, partyServiceAss);
		return driver.findElement(partyServiceAss);
	}
	
	// Distributed Instructions
	public static WebElement distInstructions(WebDriver driver) {
		By distInstructions = By.xpath("//span[contains(text(),'Distributed Instructions')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, distInstructions);
		return driver.findElement(distInstructions);
	}

}
