package com.bnym.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RenameFiles {
	
	public static void renameOutputFileName(String source,String dest,String fName)
	{
		String tempFileName=fName;
		File dir = new File(dest);
		ArrayList<String> filenames = new ArrayList<String>();
		for(File desfile : dir.listFiles())
		{
			filenames.add(desfile.getName());
		}
		//printing filenames of list
		/*for (int i = 0; i < filenames.size(); i++)
     	{
         String s = filenames.get(i);
         System.out.println("File "+i+" : "+s);
     	}
     	System.out.println("\n");*/

		File files = new File(source);
		File[] filesInDir = files.listFiles();
		for(File file:filesInDir) 
		{
			int i=0;
			String name = file.getName();
			//System.out.println("File Before Rename "+name);
			for(;;){
				if(filenames.contains(tempFileName))
				{
					
					System.out.println("file already exists with given name so modifying file name");
					i++;
					tempFileName =fName.split(".xml")[0]+"-"+i+"-"+"OUTPUT"+".xml";
					
					continue;

				}
				else
				{
					tempFileName =fName.split(".xml")[0]+"-"+i+"-"+"OUTPUT"+".xml";
					filenames.add(tempFileName);
					System.out.println("file name doesn't exist in dest folder");
					String newName = tempFileName;
					String newPath = dest + "\\" + newName;
					file.renameTo(new File(newPath));
					System.out.println(name + " changed to " + newName);
					file.delete();
					break;
				}
			}
		}
	}
	

	
	public static void renameRFPFileName(String source,String dest,String fName)
	{
		String tempFileName=fName;
		File dir = new File(dest);
		ArrayList<String> filenames = new ArrayList<String>();
		for(File desfile : dir.listFiles())
		{
			filenames.add(desfile.getName());
		}
		//printing filenames of list
		/*for (int i = 0; i < filenames.size(); i++)
     	{
         String s = filenames.get(i);
         System.out.println("File "+i+" : "+s);
     	}
     	System.out.println("\n");*/

		File files = new File(source);
		File[] filesInDir = files.listFiles();
		for(File file:filesInDir) 
		{
			int i=0;
			String name = file.getName();
			//System.out.println("File Before Rename "+name);
			for(;;){
				if(filenames.contains(tempFileName))
				{
					
					System.out.println("file already exists with given name so modifying file name");
					i++;
					tempFileName =fName+"-"+i+"-"+".xml";
					
					continue;

				}
				else
				{
					tempFileName =fName+"-"+i+"-"+".xml";
					filenames.add(tempFileName);
					System.out.println("file name doesn't exist in dest folder");
					String newName = tempFileName;
					String newPath = dest + "\\" + newName;
					file.renameTo(new File(newPath));
					System.out.println(name + " changed to " + newName);
					file.delete();
					break;
				}
			}
		}
	}
	
public static void copyFileUsingChannel(String source, String dest) throws IOException {
	    FileChannel sourceChannel = null;
	    FileChannel destChannel = null;
	    File sourcefiles = new File(source);
	    File destfiles = new File(dest);

	    try {
	        sourceChannel = new FileInputStream(sourcefiles).getChannel();
	        destChannel = new FileOutputStream(destfiles).getChannel();
	        destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
	       }finally{
	           sourceChannel.close();
	           destChannel.close();
	   }
	}

	
	public static void renameFileName(String source,String dest,String fName)
	{
		String tempFileName=fName;
		System.out.println("**************source:"+source+"\n***********tempFileName:"+tempFileName);

		File dir = new File(dest);
		System.out.println("**********dir:"+dir);
		ArrayList<String> filenames = new ArrayList<String>();
		for(File desfile : dir.listFiles())
		{
			System.out.println("***************desfile.getName():"+desfile);
			filenames.add(desfile.getName());
			System.out.println("***************after add desfile.getName():"+desfile);
		}
		//printing filenames of list
		/*for (int i = 0; i < filenames.size(); i++)
     	{
         String s = filenames.get(i);
         System.out.println("File "+i+" : "+s);
     	}
     	System.out.println("\n");*/

		File files = new File(source);
		File[] filesInDir = files.listFiles();
		for(File file:filesInDir) 
		{
			System.out.println("***************Inside foreach loop***************");
			int i=0;
			String name = file.getName();
			//System.out.println("File Before Rename "+name);
			for(;;){
				System.out.println("***************in for loop***********************");
				if(filenames.contains(tempFileName))
				{
					System.out.println("***************in if block***********************");

					System.out.println("file already exists with given name so modifying file name");
					i++;
					tempFileName =fName+"-"+i+"-"+".txt";
					
					continue;

				}
				else
				{
					System.out.println("***************in else block***********************");

					tempFileName =fName+"-"+i+"-"+".txt";
					filenames.add(tempFileName);
					System.out.println("file name doesn't exist in dest folder");
					String newName = tempFileName;
					String newPath = dest + "/" + newName;
					file.renameTo(new File(newPath));
					System.out.println(name + " changed to " + newName);
					file.delete();
					break;
				}
			}
		}
	}
	
	public static void renameISOCAMT054FileName(String source,String dest,String fName)
	{
		String tempFileName=fName;
		File dir = new File(dest);
		ArrayList<String> filenames = new ArrayList<String>();
		for(File desfile : dir.listFiles())
		{
			filenames.add(desfile.getName());
		}
		//printing filenames of list
		/*for (int i = 0; i < filenames.size(); i++)
     	{
         String s = filenames.get(i);
         System.out.println("File "+i+" : "+s);
     	}
     	System.out.println("\n");*/

		File files = new File(source);
		File[] filesInDir = files.listFiles();
		for(File file:filesInDir) 
		{
			int i=0;
			String name = file.getName();
			//System.out.println("File Before Rename "+name);
			for(;;){
				if(filenames.contains(tempFileName))
				{
					
					System.out.println("file already exists with given name so modifying file name");
					i++;
					tempFileName =fName.split(".xml")[0]+"-"+i+"ISOCAMT054"+".xml";
					continue;

				}
				else
				{
					tempFileName =fName.split(".xml")[0]+"-"+i+"-ISOCAMT054"+".xml";
					filenames.add(tempFileName);
					System.out.println("file name doesn't exist in dest folder");
					
					String newName = tempFileName;
					String newPath = dest + "\\" + newName;
					file.renameTo(new File(newPath));
					System.out.println(name + " changed to " + newName);
					file.delete();
					break;
				}
			}
		}
	}
	
	public static void renameRFIOutputFileName(String source,String dest,String fName)
	{
		String tempFileName=fName;
		File dir = new File(dest);
		ArrayList<String> filenames = new ArrayList<String>();
		for(File desfile : dir.listFiles())
		{
			filenames.add(desfile.getName());
		}
		//printing filenames of list
		/*for (int i = 0; i < filenames.size(); i++)
     	{
         String s = filenames.get(i);
         System.out.println("File "+i+" : "+s);
     	}
     	System.out.println("\n");*/

		File files = new File(source);
		File[] filesInDir = files.listFiles();
		for(File file:filesInDir) 
		{
			int i=0;
			String name = file.getName();
			//System.out.println("File Before Rename "+name);
			for(;;){
				if(filenames.contains(tempFileName))
				{
					
					System.out.println("file already exists with given name so modifying file name");
					i++;
					tempFileName =fName.split(".xml")[0]+"-"+i+"-RFIOUTPUT"+".xml";
					continue;

				}
				else
				{
					tempFileName =fName.split(".xml")[0]+"-"+i+"-RFIOUTPUT"+".xml";
					filenames.add(tempFileName);
					System.out.println("file name doesn't exist in dest folder");
					String newName = tempFileName;
					String newPath = dest + "\\" + newName;
					file.renameTo(new File(newPath));
					System.out.println(name + " changed to " + newName);
					file.delete();
					break;
				}
			}
		}
	}
	
	public static void renameRRFIOutputFileName(String source,String dest,String fName)
	{
		String tempFileName=fName;
		File dir = new File(dest);
		ArrayList<String> filenames = new ArrayList<String>();
		for(File desfile : dir.listFiles())
		{
			filenames.add(desfile.getName());
		}
		//printing filenames of list
		/*for (int i = 0; i < filenames.size(); i++)
     	{
         String s = filenames.get(i);
         System.out.println("File "+i+" : "+s);
     	}
     	System.out.println("\n");*/

		File files = new File(source);
		File[] filesInDir = files.listFiles();
		for(File file:filesInDir) 
		{
			int i=0;
			String name = file.getName();
			//System.out.println("File Before Rename "+name);
			for(;;){
				if(filenames.contains(tempFileName))
				{
					
					System.out.println("file already exists with given name so modifying file name");
					i++;
					tempFileName =fName.split(".xml")[0]+"-"+i+"-RRFIOUTPUT"+".xml";
					continue;

				}
				else
				{
					tempFileName =fName.split(".xml")[0]+"-"+i+"-RRFIOUTPUT"+".xml";
					filenames.add(tempFileName);
					System.out.println("file name doesn't exist in dest folder");
					String newName = tempFileName;
					String newPath = dest + "\\" + newName;
					file.renameTo(new File(newPath));
					System.out.println(name + " changed to " + newName);
					file.delete();
					break;
				}
			}
		}
	}
	
	public static void renameROFOutputFileName(String source,String dest,String fName)
	{
		String tempFileName=fName;
		File dir = new File(dest);
		ArrayList<String> filenames = new ArrayList<String>();
		for(File desfile : dir.listFiles())
		{
			filenames.add(desfile.getName());
		}
		//printing filenames of list
		/*for (int i = 0; i < filenames.size(); i++)
     	{
         String s = filenames.get(i);
         System.out.println("File "+i+" : "+s);
     	}
     	System.out.println("\n");*/

		File files = new File(source);
		File[] filesInDir = files.listFiles();
		for(File file:filesInDir) 
		{
			int i=0;
			String name = file.getName();
			//System.out.println("File Before Rename "+name);
			for(;;){
				if(filenames.contains(tempFileName))
				{
					
					System.out.println("file already exists with given name so modifying file name");
					i++;
					tempFileName =fName.split(".xml")[0]+"-"+i+"-ROFOUTPUT"+".xml";
					continue;

				}
				else
				{
					tempFileName =fName.split(".xml")[0]+"-"+i+"-ROFOUTPUT"+".xml";
					filenames.add(tempFileName);
					System.out.println("file name doesn't exist in dest folder");
					String newName = tempFileName;
					String newPath = dest + "\\" + newName;
					file.renameTo(new File(newPath));
					System.out.println(name + " changed to " + newName);
					file.delete();
					break;
				}
			}
		}
	}
	
	public static void renameUSRTGIncomingOutputFileName(String source,String dest,String fName)
	{
		String tempFileName=fName;
		File dir = new File(dest);
		ArrayList<String> filenames = new ArrayList<String>();
		for(File desfile : dir.listFiles())
		{
			filenames.add(desfile.getName());
		}
		//printing filenames of list
		/*for (int i = 0; i < filenames.size(); i++)
     	{
         String s = filenames.get(i);
         System.out.println("File "+i+" : "+s);
     	}
     	System.out.println("\n");*/

		File files = new File(source);
		File[] filesInDir = files.listFiles();
		for(File file:filesInDir) 
		{
			int i=0;
			String name = file.getName();
			//System.out.println("File Before Rename "+name);
			for(;;){
				if(filenames.contains(tempFileName))
				{
					
					System.out.println("file already exists with given name so modifying file name");
					i++;
					tempFileName =fName.split(".xml")[0]+"-"+i+"-USRTGIn"+".xml";
					continue;

				}
				else
				{
					tempFileName =fName.split(".xml")[0]+"-"+i+"-USRTGIn"+".xml";
					filenames.add(tempFileName);
					System.out.println("file name doesn't exist in dest folder");
					String newName = tempFileName;
					String newPath = dest + "\\" + newName;
					file.renameTo(new File(newPath));
					System.out.println(name + " changed to " + newName);
					file.delete();
					break;
				}
			}
		}
	}
	//Renaming source filename(no duplicates) and saving it in destination
	/*public static void renameISOCAMT054FileName(String source,String dest,String fName)
		{
		    File files = new File(source);
			File[] filesInDir = files.listFiles();
			for(File file:filesInDir) 
			{
			   String name = file.getName();
			   System.out.println("File Before Rename "+name);
			   String newName = fName+"-ISOCAMT054 "+System.currentTimeMillis();
			      //System.out.println(newName);
			   String newPath = dest + "\\" + newName;
			   File tmp = new File(newPath);
			   file.renameTo(new File(newPath));
			   System.out.println(name + " changed to " + newName);
			   file.delete();
			     
			}
		}*/
	//Renaming source filename(no duplicates) and saving it in destination
	/*public static void renameOutputFileName(String source,String dest,String fName)
	{
    	File files = new File(source);
	    File[] filesInDir = files.listFiles();
	    for(File file:filesInDir) 
	    {
	      String name = file.getName();
	      System.out.println("File Before Rename "+name);
	      String newName = fName+System.currentTimeMillis();
	      //System.out.println(newName);
	      String newPath = dest + "\\" + newName;
	      File tmp = new File(newPath);
	      file.renameTo(new File(newPath));
	      System.out.println(name + " changed to " + newName);
	      file.delete();
	     }
	}*/
}


