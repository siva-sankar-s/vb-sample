package com.bnym.utilities;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.bnym.pages.Login;
import com.bnym.pages.Logout;

public class MultiOSSupport {

	public static String uName = null;
	public static String uNameForUser = null;
	public static String pwdForUser = null;
	public static Logger log ;
	public static WebDriver driver=null;
	public static JavascriptExecutor js;

	public static String testcaseName = null;
	public static int TestCaseRow ;
	private static String OS = System.getProperty("os.name").toLowerCase();

	public static boolean isWindows() {

		return (OS.indexOf("win") >= 0);

	}

	public static boolean isMac() {

		return (OS.indexOf("mac") >= 0);

	}

	public static boolean isLinux() {

		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0 );

	}

	public static boolean isSolaris() {

		return (OS.indexOf("sunos") >= 0);

	}


	public static String returnEnv()
	{
		String env=null;
		//System.out.println(OS);

		if (isWindows()) {
			env="windows";
		} else if (isMac()) {
			env="mac";
		} else if (isLinux()) {
			env="linux";
		} else if (isSolaris()) {
			env="solaris";
		} else {
			env="Unsupported";
		}
		return env;


	}


	@BeforeSuite
	public static void login() throws Exception
	{
		try{
			String envDetails=returnEnv();
			System.out.println("envDetails::::"+envDetails);
			System.out.println("Inside Before suite login");
			log = Logger.getLogger("Loginlogout");

			if(envDetails.equals("windows"))
			{
				System.setProperty("webdriver.chrome.driver","Resources/chromedriver.exe");
				driver=new ChromeDriver();
			}
			else if(envDetails.equals("linux"))
			{
				System.setProperty("webdriver.chrome.driver","Resources/chromedriver");
				driver=new ChromeDriver();
			}

			js = (JavascriptExecutor)driver;

			// Launch Application
			String baseURL=ExcelUtilities.getCellDataBySheetName(Constants.File_TestCases,"LoginDetails","Application_URL", 4);
			uName=ExcelUtilities.getCellDataBySheetName(Constants.File_TestCases,"LoginDetails","Application_URL",5);
			String pwd=ExcelUtilities.getCellDataBySheetName(Constants.File_TestCases,"LoginDetails", "Application_URL",6);
			uNameForUser=ExcelUtilities.getCellDataBySheetName(Constants.File_TestCases,"User","UserApprovals",Constants.Col_userName);
			pwdForUser=ExcelUtilities.getCellDataBySheetName(Constants.File_TestCases,"User", "UserApprovals",Constants.Col_password);
			System.out.println("URL is :"+baseURL);
			System.out.println("UN is :"+uName);
			System.out.println("PWD is :"+pwd);
			Login.launch(driver,baseURL);
			Thread.sleep(3000);
			log.info("Browser launched");
			System.out.println("Browser Launched");
			Login.usrName(driver).sendKeys(uName);
			Login.password(driver).sendKeys(pwd);
			Thread.sleep(3000);
			Login.loginButton(driver).click();
			Thread.sleep(15000);
			System.out.println("User Logged In");
		} 
		catch(Exception ej)
		{
			ej.printStackTrace();
		}

	}


	@AfterSuite
	public void Userlogout() throws Exception
	{
		System.out.println("Inside Before suite logout");

		try{

			Actions act = new Actions(driver);
			act.sendKeys(Keys.ESCAPE);
			Thread.sleep(3000);
			//Logout.userprofileBtn(driver).click();

			act.moveToElement(Logout.userprofileBtn(driver,uName)).build().perform();
			Thread.sleep(2000);
			js.executeScript("arguments[0].click();", driver.findElement(By.linkText("Log Out")));


			//act.moveToElement(Logout.userprofileBtn(driver)).build().perform();
			Thread.sleep(5000);
			//driver.findElement(By.linkText("Log Out")).click();
			System.out.println("Logout successfully");
			driver.close();

		}
		catch(Exception en){
			//ExcelUtilities.setCellDataBySheetName(Constants.File_TestCases, "TestCases", TestCaseRow, Constants.Col_Elecomments,ez.getMessage());
			en.printStackTrace();

		}

	}


}
