package com.bnym.utilities;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.*;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class dataComparision extends LoginLogout{
	
	//Method to compare Debit Fund Control values
    public static List<JSONObject> compareDebitFuncontrol(String obtainedFile, String actualString, List<String> dateFields, List<String> fieldNames) throws Exception, IOException, JSONException {
			try {
				String obtainedString = new String(Files.readAllBytes(Paths.get(obtainedFile)));
				LocalDate date = LocalDate.now(); 							// get current date
				
				// change string to json object
			    JsonElement obtainedObj = JsonParser.parseString(obtainedString).getAsJsonObject();
			    JsonElement updatedObj = JsonParser.parseString(actualString).getAsJsonObject(); 
			    
			    // create map to store "key" & "value" accordingly
			    Map<String, String> matchedMap =  new HashMap<>();
			    Map<String, String> unmatchedMap =  new HashMap<>();
			    
			    // create jsonobj to store Hashmaps
			    JSONObject matchedJSONObj = new JSONObject();
			    JSONObject unmatchedJSONObj = new JSONObject();
			    
			    // iterate 'fieldNames' list
			    for (String fieldTemp : fieldNames){
			    	
			    	// Get values from both json objects and match values 
			    	if(!(((JsonObject) obtainedObj).get(fieldTemp)).toString().equals((((JsonObject) updatedObj).get(fieldTemp)).toString())) {
			    		unmatchedMap.put(fieldTemp, (((JsonObject) updatedObj).get(fieldTemp)).toString());
				    }
			    	else {
			    		matchedMap.put(fieldTemp, (((JsonObject) updatedObj).get(fieldTemp)).toString());
			    	}
			    }
			    
			    // iterate 'dateFields' list
			    for (String datesTemp : dateFields) {
		    		// remove double quotes in string("...")
			    	String updatedDate = ((JsonObject) updatedObj).get(datesTemp).toString().replaceAll("^\"|\"$", "");
		    		
			    	// check current 'date' and "updatedDate" from given string
		    		if(!(date.toString().equals(updatedDate))) {
		    			
		    			// check for 'eventDateTime' to extract 'yyyy-mm-dd'
		    			if(datesTemp=="eventDateTime") {
		    				
		    				// get string from index 1 to 11 which has 'yyyy-mm-dd' in string
		        			String eventTemp = ((JsonObject) updatedObj).get(datesTemp).toString().substring(1,11);
	
		        			// check whether both dates are equal 
		        			if (eventTemp.equals(date.toString())) {
		        				matchedMap.put(datesTemp, ((JsonObject) updatedObj).get(datesTemp).toString());
		        			}else {
		        				unmatchedMap.put(datesTemp, ((JsonObject) updatedObj).get(datesTemp).toString());
		        			}
		        		}else {
		        			unmatchedMap.put(datesTemp, ((JsonObject) updatedObj).get(datesTemp).toString());
		        		}
		    		}else {
		    			matchedMap.put(datesTemp, ((JsonObject) updatedObj).get(datesTemp).toString());
			    	}
		    	}
			    
			    // store both hashmaps to jsonobjects
			    matchedJSONObj.put("Matched", matchedMap);
			    unmatchedJSONObj.put("Unmatched", unmatchedMap);
	
			    // create list and combine those jsonobjects into it.
			    List<JSONObject> listOfJSONObj = Arrays.asList(matchedJSONObj,unmatchedJSONObj);
			    
				return listOfJSONObj;
			}
			catch(Exception e) {
				System.out.println(e.getLocalizedMessage());
				throw new Exception("Error Occured In compareDebitFuncontrol(): " + e.getMessage());
			}
	    }
    
// Method to compare Finacle posting Json    
		public static List<JSONObject> compareFinaclePosting(String obtainedFile, String actualString, List<String> fieldNames,
				List<String> dateFields, List<String> additionalAttributesKeys, List<String> movementRequestsKeys)
				throws IOException, JSONException, Exception {
			try {
				String obtainedString = new String(Files.readAllBytes(Paths.get(obtainedFile)));
				LocalDate date = LocalDate.now(); // get current date

				// change string to json object
				JsonElement obtainedObj = JsonParser.parseString(obtainedString).getAsJsonObject();

				JsonElement obtainedadditionalAttObj = ((JsonObject) obtainedObj).get("additionalAttributes");
				String obtainedmovementRequestsStr = ((JsonObject) obtainedObj).get("movementRequests").toString();
				JsonObject obtainedmovementReqObj = JsonParser
						.parseString(obtainedmovementRequestsStr.substring(1, obtainedmovementRequestsStr.length() - 1))
						.getAsJsonObject();

				JsonElement updatedObj = JsonParser.parseString(actualString).getAsJsonObject();

				JsonElement updatedadditionalAttObj = ((JsonObject) updatedObj).get("additionalAttributes");
				String updatedmovementRequestsStr = ((JsonObject) updatedObj).get("movementRequests").toString();
				JsonObject updatedmovementReqObj = JsonParser
						.parseString(updatedmovementRequestsStr.substring(1, updatedmovementRequestsStr.length() - 1))
						.getAsJsonObject();

				// create map to store "key" & "value" accordingly
				Map<String, String> matchedMap = new HashMap<>();
				Map<String, String> unmatchedMap = new HashMap<>();
				Map<String, String> datesMap = new HashMap<>();

				// create jsonobj to store Hashmaps
				JSONObject matchedJSONObj = new JSONObject();
				JSONObject unmatchedJSONObj = new JSONObject();

				// iterate 'fieldNames' list
				for (String fieldTemp : fieldNames) {
					try {
						if (!(dateFields.contains(fieldTemp))) {
							// Get values from both json objects and match values
							if (!(((JsonObject) obtainedObj).get(fieldTemp)).toString()
									.equals((((JsonObject) updatedObj).get(fieldTemp)).toString())) {
								unmatchedMap.put(fieldTemp, (((JsonObject) updatedObj).get(fieldTemp)).toString());
							} else {
								matchedMap.put(fieldTemp, (((JsonObject) updatedObj).get(fieldTemp)).toString());
							}
						} else {
							datesMap.put(fieldTemp, (((JsonObject) updatedObj).get(fieldTemp)).toString());
						}
					} catch (Exception e) {
						e.printStackTrace();
						System.out.println(e.toString());
					}
				}

				for (String fieldTemp : additionalAttributesKeys) {
					try {
						if (!(dateFields.contains(fieldTemp))) {
							// Get values from both json objects and match values
							if (!(((JsonObject) obtainedadditionalAttObj).get(fieldTemp)).toString()
									.equals((((JsonObject) updatedadditionalAttObj).get(fieldTemp)).toString())) {
								unmatchedMap.put(fieldTemp,
										(((JsonObject) updatedadditionalAttObj).get(fieldTemp)).toString());
							} else {
								matchedMap.put(fieldTemp,
										(((JsonObject) updatedadditionalAttObj).get(fieldTemp)).toString());
							}
						} else {
							datesMap.put(fieldTemp, (((JsonObject) updatedadditionalAttObj).get(fieldTemp)).toString());
						}
					} catch (Exception e) {
						e.printStackTrace();
						System.out.println(e.toString());
					}
				}

				for (String fieldTemp : movementRequestsKeys) {
					try {
						// Get values from both json objects and match values
						if (!(dateFields.contains(fieldTemp))) {
							if (!(((JsonObject) obtainedmovementReqObj).get(fieldTemp)).toString()
									.equals((((JsonObject) updatedmovementReqObj).get(fieldTemp)).toString())) {
								unmatchedMap.put(fieldTemp,
										(((JsonObject) updatedmovementReqObj).get(fieldTemp)).toString());
							} else {
								matchedMap.put(fieldTemp, (((JsonObject) updatedmovementReqObj).get(fieldTemp)).toString());
							}
						} else {
							datesMap.put(fieldTemp, (((JsonObject) updatedmovementReqObj).get(fieldTemp)).toString());
						}

					} catch (Exception e) {
						e.printStackTrace();
						System.out.println(e.toString());
					}
				}

				Iterator<?> it = datesMap.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry pair = (Map.Entry) it.next();

					// get string from index 1 to 11 which has 'yyyy-mm-dd' in string
					String eventTemp = (pair.getValue().toString()).substring(1, 11);

					// check whether both dates are equal
					if (eventTemp.equals(date.toString())) {
						matchedMap.put(pair.getKey().toString(), pair.getValue().toString());
					} else {
						unmatchedMap.put(pair.getKey().toString(), pair.getValue().toString());
					}
				}

				// store both hashmaps to jsonobjects
				matchedJSONObj.put("Matched", matchedMap);
				unmatchedJSONObj.put("Unmatched", unmatchedMap);

				// create list and combine those jsonobjects into it.
				List<JSONObject> listOfJSONObj = Arrays.asList(matchedJSONObj, unmatchedJSONObj);

				return listOfJSONObj;
				
			}catch(Exception e) {
				System.out.println(e.getLocalizedMessage());
				throw new Exception("Error Occured In compareFinaclePosting(): " + e.getMessage());
			}
		}
		
		// Compare mandate field values in BEC notification
		
		public static List<JSONObject> compareBECMandateFields(String actualBECString, List<String> fieldNames, List<String> doMatch) throws Exception {
			try {
				String extractedBECTags = null;
				
				// create maps to store matched and unmatched "keys" and "values"
				Map<String,String> matchedMap = new LinkedHashMap<String,String>();
				Map<String,String> unmatchedMap = new LinkedHashMap<String,String>();
				
				// create jsonobj to store Hashmaps
				JSONObject matchedMapObj = new JSONObject();
			    JSONObject unmatchedMapObj = new JSONObject();
			    
			    // create "doMatchMap" map to link both "keys" & "Values" - entered by client
			    Map<String,String> doMatchMap = new LinkedHashMap<String,String>();
				for (int i=0; i<fieldNames.size(); i++) {
			 		doMatchMap.put(fieldNames.get(i), doMatch.get(i));
			 	}
				
				// process with "Whole string in UI" to get "Mandate Tag values"
				Pattern wholePattern = Pattern.compile("<Mandate>(.*?)</Mandate>", Pattern.DOTALL);
				Matcher wholeMatcher = wholePattern.matcher(actualBECString);
				while (wholeMatcher.find()) {
					extractedBECTags = (wholeMatcher.group(1)).replaceAll("\\s+"," ").trim();
				}
				
				// process with "Mandate Tag values" to get "Each tag and tag values inside of it"
				for (String fieldName : fieldNames) {
					// Pattern to identify Tags
					Pattern mandatePattern = Pattern.compile("<"+ fieldName +">(.*?)</"+ fieldName +">", Pattern.DOTALL);
					Matcher mandateMatcher = mandatePattern.matcher(extractedBECTags);
					while (mandateMatcher.find()) {
						if ((mandateMatcher.group(1)).equals(doMatchMap.get(fieldName))) {
							matchedMap.put(fieldName, mandateMatcher.group(1));
						} else {
							unmatchedMap.put(fieldName, mandateMatcher.group(1));
						}
					}
				}
				
				// store both hashmaps to jsonobjects
			    matchedMapObj.put("Matched", matchedMap);
			    unmatchedMapObj.put("Unmatched", unmatchedMap);

			    // create list and combine those jsonobjects into it.
			    List<JSONObject> listOfJSONObj = Arrays.asList(matchedMapObj,unmatchedMapObj);
			    
			    return listOfJSONObj;
			} catch (Exception e) {
				System.out.println(e.getLocalizedMessage());
				throw new Exception("Error Occured In compareBECMandateFields(): " + e.getMessage());
			}
		}

}
