package com.bnym.utilities;



import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DBUTILS {
	
	static String connection_driver=null;
	static String driverClassName=null;
	static String url=null;
	static String username=null;
	static String password=null;
	static Connection con=null;
	static ResultSet rs3=null;
	static ResultSet rs=null;
	static String instructionId;
	static String paymentid;
	static String resultData;
	private static final int BUFFER_SIZE = 8192;
	
	
	/*public static void readDataSources(String path) throws Exception
	{
		File inputFile = new File(path);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(inputFile);
		doc.getDocumentElement().normalize();
		System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
		//NodeList data= doc.getElementsByTagName("firstname");
		NodeList nList1 = doc.getElementsByTagName("Resource");

		//System.out.println("----------------------------");

		for (int temp = 0; temp < nList1.getLength(); temp++)
		{
			Node nNode = nList1.item(temp);
			System.out.println("\nCurrent Element :" + nNode.getNodeName());

			if (nNode.getNodeType() == Node.ELEMENT_NODE)
			{

				Element eElement = (Element) nNode;
				//System.out.println("tr:Id- " + eElement.getElementsByTagName("tr:Id").item(0).getTextContent());
				driverClassName =eElement.getAttribute("driverClassName");
				url=eElement.getAttribute("url");
				username=eElement.getAttribute("username");
				password=eElement.getAttribute("password");


				System.out.println("connection-driver : "+driverClassName);
				System.out.println("url :"+url);
				System.out.println("username :"+username);
				System.out.println("password :"+password);
			}
		}
	}*/
	
	
	
	public static Connection getDBConnection(String path) throws SQLException, ClassNotFoundException, ParserConfigurationException, Exception, Exception{
		
		File inputFile = new File(path);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(inputFile);
		doc.getDocumentElement().normalize();
		System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
		NodeList nList1 = doc.getElementsByTagName("Resource1");
		for (int temp = 0; temp < nList1.getLength(); temp++)
		{
			Node nNode = nList1.item(temp);
			System.out.println("\nCurrent Element :" + nNode.getNodeName());

			if (nNode.getNodeType() == Node.ELEMENT_NODE)
			{

				Element eElement = (Element) nNode;
				//System.out.println("tr:Id- " + eElement.getElementsByTagName("tr:Id").item(0).getTextContent());
				driverClassName =eElement.getAttribute("driverClassName");
				url=eElement.getAttribute("url");
				username=eElement.getAttribute("username");
				password=eElement.getAttribute("password");


				/*System.out.println("connection-driver : "+driverClassName);
				System.out.println("url :"+url);
				System.out.println("username :"+username);
				System.out.println("password :"+password);*/
			}
		}
		
		Connection con=null;

		if(con==null){
			Class.forName(driverClassName);  
			con=DriverManager.getConnection(url,username,password);
		}
		return con;

	}
	
	public static Connection getAQDBConnection(String path) throws SQLException, ClassNotFoundException, ParserConfigurationException, Exception, Exception{
		
		File inputFile = new File(path);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(inputFile);
		doc.getDocumentElement().normalize();
		System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
		NodeList nList1 = doc.getElementsByTagName("Resource2");
		for (int temp = 0; temp < nList1.getLength(); temp++)
		{
			Node nNode = nList1.item(temp);
			System.out.println("\nCurrent Element :" + nNode.getNodeName());

			if (nNode.getNodeType() == Node.ELEMENT_NODE)
			{

				Element eElement = (Element) nNode;
				//System.out.println("tr:Id- " + eElement.getElementsByTagName("tr:Id").item(0).getTextContent());
				
				String dbName=eElement.getAttribute("name");
				
				if(dbName.equals("jdbc/aqDB"))
				{
					driverClassName =eElement.getAttribute("driverClassName");
					url=eElement.getAttribute("url");
					username=eElement.getAttribute("username");
					System.out.println("User name:  "+username);
					password=eElement.getAttribute("password");
					System.out.println("password:  "+password);
					
				}
				


				/*System.out.println("connection-driver : "+driverClassName);
				System.out.println("url :"+url);
				System.out.println("username :"+username);
				System.out.println("password :"+password);*/
			}
		}
		
		Connection con=null;

		if(con==null){
			Class.forName(driverClassName);  
			con=DriverManager.getConnection(url,username,password);
		}
		return con;

	}
	
	
	
	/*public static String getTransportName(String INSTRUCTIONID) throws SQLException, ClassNotFoundException
	{

		//con=getDBConnection();
		Statement st=con.createStatement();
		rs3=st.executeQuery("SELECT TRANSPORTNAME from INSTRUCTIONRAWDATA where INSTRUCTIONID='"+INSTRUCTIONID+"'");
		if(rs3.next())
		{
			TRANSPORTNAME=rs3.getString("TRANSPORTNAME");
			System.out.println(TRANSPORTNAME);
		}
		rs3.close();
		st.close();
		return TRANSPORTNAME;
	}*/
	
	
	
	public static String getInsId(String path,String transportName) throws SQLException, Exception
	{
		con=getDBConnection(path);
		Statement st=con.createStatement();
		rs3=st.executeQuery("SELECT INSTRUCTIONID from INSTRUCTIONRAWDATA where TRANSPORTNAME ='"+transportName+"' and UNSTRUCTUREDINFO IS NOT NULL");
		if(rs3.next())
		{
			instructionId=rs3.getString("INSTRUCTIONID");
			//System.out.println(instructionId);
		}
		rs3.close();
		st.close();
		return instructionId;
	
		
	}
	
	public static String getPaymentId(String path,String insid) throws SQLException, Exception
	{
		con=getDBConnection(path);
		Statement st=con.createStatement();
		rs3=st.executeQuery("SELECT PAYMENTID from PAYMENTCONTROLDATA where INSTRUCTIONID ='"+insid+"'");
		if(rs3.next())
		{
			paymentid=rs3.getString("PAYMENTID");
			System.out.println("paymentid:::::"+paymentid);
		}
		rs3.close();
		st.close();
		return paymentid;
	
		
	}
	
	public static void executeQuery(String path,String query) throws SQLException, Exception
	{
		try{
			con=getDBConnection(path);
			Statement st=con.createStatement();
			st.executeQuery(query);
			
			//rs3.close();
			st.close();
			
		}
		catch(SQLException se)
		{
			se.printStackTrace();
		}
		
		
	
		
	}
	
	public static void executeAQQuery(String path,String query) throws SQLException, Exception
	{
		try{
			con=getAQDBConnection(path);
			Statement st=con.createStatement();
			st.executeQuery(query);
			//st.executeUpdate(query);
			//rs3.close();
			st.close();
			
		}
		catch(SQLException se)
		{
			se.printStackTrace();
		}
		
		
	
		
	}
	
	public static String fetchDBData(String path,String query,String colName) throws SQLException, Exception
	{
		ResultSet rs=null;
		String resultData=null;
		con=getDBConnection(path);
		Statement st=con.createStatement();
		System.out.println("query>>>"+query);
		rs=st.executeQuery(query);
		if(rs.next())
		{
			resultData=rs.getString(colName);
			//System.out.println("resultData>>>"+resultData);
		}
		
		rs.close();
		st.close();
		return resultData;
	
		
	}
	
	
	public static String getPhyFileInsId(String path,String transportName) throws SQLException, Exception
	{
		con=getDBConnection(path);
		Statement st=con.createStatement();
		rs3=st.executeQuery("SELECT INSTRUCTIONID from INSTRUCTIONRAWDATA where TRANSPORTNAME ='"+transportName+"' and UNSTRUCTUREDINFO IS NULL");
		if(rs3.next())
		{
			instructionId=rs3.getString("INSTRUCTIONID");
			System.out.println(instructionId);
		}
		rs3.close();
		st.close();
		return instructionId;
	
		
	}
	
	public static void getACKByInsId(String datasourcePath,String ackPath,String phyFileName,String insId) throws ClassNotFoundException, SQLException, ParserConfigurationException, Exception
	{
		con=getDBConnection(datasourcePath);
		Statement st=con.createStatement();
		try 
		{
			rs3=st.executeQuery("SELECT * from ACKDETAILS where INSTRUCTIONID='"+insId+"'");
			
			while(rs3.next())
			{
				
				if(rs3.getString(6)!=null)
				{
					Blob b=rs3.getBlob("ACKDATA");
					InputStream inputStream = b.getBinaryStream();
					File dest=new File(ackPath+"//"+phyFileName);
					OutputStream outputStream = new FileOutputStream(dest);
					//OutputStream outputStream = new FileOutputStream("D:\\BNYMRTP\\BNYMGETDB\\DBDATA2.txt");

					int bytesRead = -1;
					byte[] buffer = new byte[BUFFER_SIZE];
					while ((bytesRead = inputStream.read(buffer)) != -1) 
					{
						outputStream.write(buffer, 0, bytesRead);
						inputStream.close();
						outputStream.close();

						//System.out.println(rs.getString("UNIQUEOUTPUTREFERENCE")+" POD Data Fetched");


					}


				}
				else
				{
					System.out.println("ACK/NACK file not found");
				}
				
			}
			rs3.close();
			
		} 
		catch (SQLException e) 
		{

			e.printStackTrace();
		}
		
		con.close();


	}
	
	
	public static void getEgressViewOutputs(String datasourcePath,String ackPath,String query) throws ClassNotFoundException, SQLException, ParserConfigurationException, Exception
	{
		con=getDBConnection(datasourcePath);
		Statement st=con.createStatement();
		try 
		{
			rs3=st.executeQuery(query);
			
			while(rs3.next())
			{
				
				if(rs3.getString(3)!=null)
				{
					Blob b=rs3.getBlob("FILE");
					InputStream inputStream = b.getBinaryStream();
					File dest=new File(ackPath+"//"+rs3.getString("FILENAME"));
					OutputStream outputStream = new FileOutputStream(dest);
					//OutputStream outputStream = new FileOutputStream("D:\\BNYMRTP\\BNYMGETDB\\DBDATA2.txt");

					int bytesRead = -1;
					byte[] buffer = new byte[BUFFER_SIZE];
					while ((bytesRead = inputStream.read(buffer)) != -1) 
					{
						outputStream.write(buffer, 0, bytesRead);
						inputStream.close();
						outputStream.close();


					}


				}
				else
				{
					System.out.println("File not found");
				}
				
			}
			
		} 
		catch (SQLException e) 
		{

			e.printStackTrace();
		}
		
		con.close();


	}
	
	
	
	
	
	
	
	
	
	
	

}
