package com.bnym.utilities;

public class Constants {

	/* ++++++++++++++++++++ Do not change the CONSTANTS used in Utilities package ++++++++++++++++++++ */
	
	public static final String Path_TestData = "TestData";
	
	public static String DownloadsPath = "Downloads";
	
	public static final String baseRESTURL = "https://ec2-3-208-75-199.compute-1.amazonaws.com:8443/VolPayRest/rest/v2";

	public static String extendedreport = "reports\\Report.html";
	
	// wait constants
	public static int tooshort_sleep = 2000;
	public static int short_sleep = 5000;
	public static int short_implicit = 5;
	public static int short_explicit = 10;
	
	public static int avg_sleep = 10000;
	public static int avg_implicit = 10;
	public static int avg_explicit = 30;
	
	public static int long_sleep = 20000;
	public static int long_implicit = 15;
	public static int long_explicit = 50;


	public static String Deployment = "E:\\GS_Environment\\apache-tomcat\\VPH_messages\\Input";

	public static final String File_TestCases = Path_TestData + "\\" + "TestCases.xlsx";
	public static final String File_TestData = Path_TestData + "\\" + "TestData.xlsx";

	// Testcases excel sheet
	public static final int Col_testcaseName = 1;
	public static final int Col_sampleDirectory = 2;
	public static final int Col_status = 3;
	public static final int Col_exeDateTime = 4;
	public static final int Col_outputsPath = 5;
	public static final int Col_comments = 6;
	public static final int Col_AssertionComments = 7;
	public static final int Col_Elecomments = 6;
	public static final int Col_userName = 5;
	public static final int Col_password = 6;
	
	// Application_Info sheet columns
	public static final int Col_ApplicationDetails = 1;
	public static final int Col_ApplicationData = 2;

	// Statements sheet column names
	public static final int Col_statement_Party = 2;
	public static final int Col_statement_Service = 3;
	public static final int Col_statement_InputFormat = 4;
	public static final int Col_statement_PSA = 5;
	public static final int Col_statement_InputFileName = 6;
	public static final int Col_statement_InputFileName2=7;
	public static final int Col_Return_InputFileName=7;

	// view details
	public static final int Col_View_sampleName = 1;
	public static final int Col_View_transactionID = 2;
	public static final int Col_View_Actual_view_details = 3;
	public static final int Col_View_Expected_view_details = 4;
	public static final int Col_View_status = 5;

	// BPSUI Entities test data
	public static final int status = 12;

	public static final int view_status = 5;
	// ActiveMQ TRIGGERS sheet of Test_Data workbook column names
	public static final int Col_QueueName = 1;
	public static final int Col_JsonMsg = 2;

	// Test data sheet column names
	public static final int roleName = 2;

	// FileUpload sheet columns
	public static final int Col_Party = 2;
	public static final int Col_Service = 3;
	public static final int Col_InputFormat = 4;
	public static final int Col_PSA = 5;
	public static final int Col_pmtConfStatus = 6;
	public static final int Col_CT_In_IncomingPaymentConfirmationStatus = 7;

	public static final int Col_Party2 = 7;
	public static final int Col_Service2 = 8;
	public static final int Col_InputFormat2 = 9;
	public static final int Col_PSA2 = 5;
	public static final int Col_InputFileName2 = 10;
	public static final int Col_InputFileName3 = 11;

	// Advanced Search Columns
	public static final int Col_SearchPSA = 5;
	public static final int Col_MOP = 7;
	public static final int Col_PaymentStatus = 14;
	public static final int Col_InstructionId = 15;
	public static final int Col_IMAD=16; 
	public static final int Col_pRefrence=10;

	//DB Output Folder Path
	public static final String Path_DBData = "DBOutput";
	
	// Test Case Roles column names
	public static final int Col_TestcaseName = 1;
	public static final int Col_RoleName = 2;

	// MT940statements Sheet columns
	public static final int Col_accountNumber = 3;
	public static final int Col_OrigInstructionId = 2;
		
	/* ++++++++++++++++++++ Project specified CONSTANTS - Start ++++++++++++++++++++ */

	// your constants here

	
	/* ++++++++++++++++++++ Project specified CONSTANTS - End ++++++++++++++++++++ */
	
	
}
