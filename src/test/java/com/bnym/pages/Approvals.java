package com.bnym.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.bnym.utilities.Constants;
import com.bnym.utilities.WaitLibrary;
import com.paulhammant.ngwebdriver.ByAngular;
import com.paulhammant.ngwebdriver.ByAngularRepeater;

public class Approvals {

	public static WebElement element = null;
	
	public static By approvalTableData = ByAngular.repeater("data in restData");
			
	//Clicking Filter Icon
	public static WebElement clickFilterIcon(WebDriver driver) {
		By filterIcon = By.xpath("//button[contains(@tooltip,'Filter')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, filterIcon);
		return driver.findElement(filterIcon);
	}
	
	// Click Approve button
	public static WebElement clickApproveBtn(WebDriver driver) {
		By approveBtn = By.xpath("//input[contains(@ng-click,'Approved = true')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, approveBtn);
		return driver.findElement(approveBtn);
	}
	
	//Enter search params
	public static WebElement filterWithKeyword(WebDriver driver) {
		By searchBox = By.xpath("//input[contains(@name,'keywordSearch')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, searchBox);
		return driver.findElement(searchBox);
	}
	
	//Get Approval Table Data
	public static WebElement approvalTableData(WebDriver driver, int row) {
		By appTableData = ((ByAngularRepeater) approvalTableData).row(0);
		WebElement elem = driver.findElement(appTableData);
		By dat = ByAngular.repeater("val in sortMenu");
		return elem.findElements(dat).get(6);
	}
	
	//Get Notes input
	public static WebElement notes(WebDriver driver) {
		By notes = By.xpath("//textarea[@id='idforNotes']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, notes);
		return driver.findElement(notes);
	}
	
	// Get Additional Approval Data Elements
	public static List<WebElement> additionApprElems (WebDriver driver){
		By elemRepeater = By.xpath("//tr[@ng-repeat='aDDvalues in FinalDataArraypcd track by $index']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, elemRepeater);
		return driver.findElements(elemRepeater);
	}
	
	public static WebElement addtApprField(WebDriver driver, int row) {
		By apprField = By.xpath("//tr[@ng-repeat='aDDvalues in FinalDataArraypcd track by $index']["+row+"]/td[1]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, apprField);
		return driver.findElement(apprField);
	}
	
	public static WebElement addtApprData(WebDriver driver, int row) {
		By apprData = By.xpath("//tr[@ng-repeat='aDDvalues in FinalDataArraypcd track by $index']["+row+"]/td[2]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, apprData);
		return driver.findElement(apprData);
	}

}
