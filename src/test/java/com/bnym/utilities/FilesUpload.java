package com.bnym.utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.comparator.LastModifiedFileComparator;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class FilesUpload {

	public static void copyFile(String src, String dest) throws IOException, Exception {

		File files = new File(src);
		File Dest = new File(dest);

		System.out.println(files.getAbsolutePath());

		try {
			FileUtils.copyFileToDirectory(files, Dest);
			Thread.sleep(3000);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	public static String executeRESTCommand(String command) {
		String s = null;

		try {
			Runtime rt = Runtime.getRuntime();

			Process proc = rt.exec(command);

			BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));

			// System.out.println("Here is the standard output of the command:\n");

			while ((s = stdInput.readLine()) != null) {
				System.out.println(s);
			}

			return s;
		} catch (Exception e) {
			return e.getMessage();
		}

	}

	public static void copyFileToDir(String src, String dest) throws IOException, Exception {

		File files = new File(src);
		File Dest = new File(dest);

		// System.out.println(files.getAbsolutePath());

		try {
			FileUtils.copyFileToDirectory(files, Dest);

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

	}

	/*
	 * public static void copyFilesToConf(String src)throws IOException, Exception {
	 * 
	 * File files = new File(src); File Dest = new
	 * File(VOCCONSTANTS.OUT_US_TCH_RTP_CONF);
	 * 
	 * System.out.println(files.getAbsolutePath());
	 * 
	 * try { FileUtils.copyFileToDirectory(files, Dest); Thread.sleep(3000); }
	 * catch(Exception e) { System.out.println(e.getMessage()); }
	 * 
	 * 
	 * 
	 * 
	 * }
	 */
	/*
	 * public static void CopyFilesToALL_RPX_TCH_IN(String src) {
	 * 
	 * File files = new File(src); File Dest = new
	 * File(VOCCONSTANTS.ALL_RPX_TCH_IN);
	 * 
	 * System.out.println(files.getAbsolutePath());
	 * 
	 * try { FileUtils.copyFileToDirectory(files, Dest); Thread.sleep(3000); }
	 * catch(Exception e) { System.out.println(e.getMessage()); }
	 * 
	 * 
	 * 
	 * 
	 * }
	 */

	public static File returnFile(String src) {

		File theNewestFile = null;
		File dir = new File(src);
		// FileFilter fileFilter = new WildcardFileFilter("*." + ext);
		File[] files = dir.listFiles();

		if (files.length > 0) {
			/** The newest file comes first **/
			Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
			theNewestFile = files[0];
		}

		return theNewestFile;
	}

	public static void uploadFileByWinSCP(String sourceLocalFilepath, String destinationServerFilePath)
			throws JSchException, SftpException, IOException, InterruptedException {
		Properties prop = readPropertiesFile(System.getProperty("user.dir") + "\\TestData\\GlobalData.properties");
		String home = "/home/VolPay/apache-tomcat/VPH_messages/Input/";
		JSch jsch = new JSch();
		jsch.addIdentity(System.getProperty("user.dir") + "\\TestData\\vinbytes.ppk");
		Session session = jsch.getSession(prop.getProperty("userName"), prop.getProperty("host"),
				Integer.parseInt(prop.getProperty("port")));
		session.setConfig("PreferredAuthentications", "publickey");
		session.setConfig("StrictHostKeyChecking", "no");
		session.setConfig("UseDNS", "no");
		session.connect();
		Channel channel = session.openChannel("shell");
		channel.setInputStream(System.in);
		channel.setOutputStream(System.out);
		channel.connect(3 * 1000);
		ChannelSftp sftp = (ChannelSftp) session.openChannel("sftp");
		System.out.println("Connecting winSCP");
		sftp.connect(3 * 1000);
		System.out.println("Changing directory to home");
		sftp.cd(home);
		System.out.println("getting Local file");
		File localFile = new File(System.getProperty("user.dir") + "\\TestData\\Samples\\" + sourceLocalFilepath);
		InputStream inputStream = new FileInputStream(localFile);
		File f = new File(localFile.getName());
		System.out.println("Uploading file");
		sftp.put(inputStream, home + f.getName());
		sftp.rename(home + f.getName(), home + destinationServerFilePath + f.getName());
		sftp.disconnect();
		sftp.exit();
		System.out.println("File Uploaded");
		Thread.sleep(2000);
	}

	public static Properties readPropertiesFile(String fileName) throws IOException {
		FileInputStream fis = null;
		Properties prop = null;
		try {
			fis = new FileInputStream(fileName);
			prop = new Properties();
			prop.load(fis);
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} finally {
			fis.close();
		}
		return prop;
	}

}
