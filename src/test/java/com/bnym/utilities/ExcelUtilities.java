package com.bnym.utilities;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtilities {

	public static FileInputStream ExcelFile = null;
	public static FileOutputStream fileOut = null;
	public static XSSFSheet ExcelWSheet = null;

	public static XSSFWorkbook ExcelWBook = null;

	public static XSSFCell Cell = null;

	public static XSSFRow Row = null;

	static DataFormatter formatter = null;

	// This method is to set the File path and to open the Excel file, Pass Excel
	// Path and Sheetname as Arguments to this method

	public static void setExcelFile(String Path, String SheetName) throws Exception {

		try {

			// Open the Excel file

			ExcelFile = new FileInputStream(Path);

			// Access the required test data sheet

			ExcelWBook = new XSSFWorkbook(ExcelFile);

			ExcelWSheet = ExcelWBook.getSheet(SheetName);

			// Log.info("Excel sheet opened");

		} catch (Exception e) {

			throw (e);
		}

	}

	// This method is to read the test data from the Excel cell, in this we are
	// passing parameters as Row num and Col num

	public static String getCellData(int RowNum, int ColNum) throws Exception {

		try {

			Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);

			String CellData = Cell.getStringCellValue();

			return CellData;

		} catch (Exception e) {

			return "";

		}

	}

	public static String getStaticCellDataBySheetName(String path, String SheetName, String TestCaseName, int ColNum)
			throws Exception {

		try {

			// int TestCaseRow=ExcelUtilities.getRowContainsBySheetName(path,SheetName,
			// TestCaseName, Constants.Col_TestCaseName);
			int TestCaseRow = ExcelUtilities.getRowContainsBySheetName(path, SheetName, TestCaseName,
					Constants.Col_ApplicationDetails);
			Cell = ExcelWSheet.getRow(TestCaseRow).getCell(ColNum);

			String CellData = Cell.getStringCellValue();

			return CellData;
		} catch (Exception e) {

			return e.getLocalizedMessage();

		} finally {
			ExcelFile.close();
		}

	}

	public static String getCellDataBySheetName(String path, String SheetName, String TestCaseName, int ColNum)
			throws Exception {

		try {

			String CellData;
			// setExcelFile(path,SheetName);
			int TestCaseRow = ExcelUtilities.getRowContainsBySheetName(path, SheetName, TestCaseName,
					Constants.Col_testcaseName);
			// int TestCaseRow=ExcelUtilities.getRowContainsBySheetName(path,SheetName,
			// TestCaseName, Constants.Col_ApplicationDetails);

			// System.out.println("Row number in getcelldatafn is
			// :"+SheetName+"::"+TestCaseRow);
			Cell = ExcelWSheet.getRow(TestCaseRow).getCell(ColNum);
			CellType cellType = Cell.getCellTypeEnum();
			if(cellType.toString().equals("NUMERIC")) {
				Double d = Cell.getNumericCellValue();				
				long intCellData = (long) Cell.getNumericCellValue();
				CellData = String.valueOf(intCellData);
				} else {
				CellData = Cell.getStringCellValue();
				}
			return CellData;

		} catch (Exception e) {

			return "";

		} finally {
			ExcelFile.close();
		}

	}

	public static String getCellDataBySheetNameByrows(String path, String SheetName, int rowNum, int ColNum)
			throws Exception {

		try {
			ExcelFile = new FileInputStream(path);
			ExcelWBook = new XSSFWorkbook(ExcelFile);
			ExcelWSheet = ExcelWBook.getSheet(SheetName);
			Cell = ExcelWSheet.getRow(rowNum).getCell(ColNum);
			String CellData = Cell.getStringCellValue();

			return CellData;

		} catch (Exception e) {

			return "";

		} finally {
			ExcelFile.close();
		}

	}

	public static String getJSONDataBySheetName(String path, String SheetName, String TestCaseName, int ColNum)
			throws Exception {

		try {

			// setExcelFile(path,SheetName);
			int TestCaseRow = ExcelUtilities.getRowContainsBySheetName(path, SheetName, TestCaseName,
					Constants.Col_QueueName);
			// int TestCaseRow=ExcelUtilities.getRowContainsBySheetName(path,SheetName,
			// TestCaseName, Constants.Col_ApplicationDetails);

			// System.out.println("Row number in getcelldatafn is
			// :"+SheetName+"::"+TestCaseRow);
			Cell = ExcelWSheet.getRow(TestCaseRow).getCell(ColNum);

			String CellData = Cell.getStringCellValue();

			return CellData;

		} catch (Exception e) {

			return "";

		} finally {
			ExcelFile.close();
		}

	}

	// This method is to write in the Excel cell, Row num and Col num are the
	// parameters

	public static void setCellData(String Result, int RowNum, int ColNum) throws Exception {

		try {

			Row = ExcelWSheet.getRow(RowNum);

			// Cell = Row.getCell(ColNum, Row.RETURN_BLANK_AS_NULL);
			Cell = Row.getCell(ColNum);
			if (Cell == null) {

				Cell = Row.createCell(ColNum);

				Cell.setCellValue(Result);

				XSSFCellStyle style1 = ExcelWBook.createCellStyle();
				style1.setFillForegroundColor(new XSSFColor(new java.awt.Color(128, 0, 128)));
				Cell.setCellStyle(style1);
				// style1.setFillPattern(CellStyle.SOLID_FOREGROUND);
				/*
				 * XSSFCellStyle my_style = ExcelWBook.createCellStyle();
				 * 
				 * my_style.setFillForegroundColor(IndexedColors.BLUE.getIndex());
				 * my_style.setFillBackgroundColor(IndexedColors.RED.getIndex());
				 * Cell.setCellStyle(my_style);
				 */

			} else {
				Cell.setCellValue(Result);

				XSSFCellStyle style1 = ExcelWBook.createCellStyle();
				style1.setFillForegroundColor(new XSSFColor(new java.awt.Color(128, 0, 128)));
				Cell.setCellStyle(style1);

			}

			// Constant variables Test Data path and Test Data file name

			FileOutputStream fileOut = new FileOutputStream(Constants.Path_TestData + Constants.File_TestCases);

			ExcelWBook.write(fileOut);

			fileOut.flush();

			fileOut.close();

		} catch (Exception e) {

			throw (e);

		}

	}

	public static void setCellDataBySheetName(String path, String SheetName, int RowNum, int ColNum, String Result)
			throws Exception {

		try {

			// Open the Excel file

			FileInputStream ExcelFile = new FileInputStream(path);

			// Access the required test data sheet

			ExcelWBook = new XSSFWorkbook(ExcelFile);

			ExcelWSheet = ExcelWBook.getSheet(SheetName);

			// Log.info("Excel sheet opened");

		} catch (Exception ex) {

			throw (ex);
		}

		try {

			Row = ExcelWSheet.getRow(RowNum);

			// Cell = Row.getCell(ColNum, Row.RETURN_BLANK_AS_NULL);
			Cell = Row.getCell(ColNum);
			if (Cell == null) {

				Cell = Row.createCell(ColNum);
				System.out.println("Excel result valueeee is:::::" + Result);

				Cell.setCellValue(Result);

				/*
				 * XSSFCellStyle style1 = ExcelWBook.createCellStyle();
				 * style1.setFillForegroundColor(new XSSFColor(new java.awt.Color(128, 0,
				 * 128))); Cell.setCellStyle(style1);
				 */
				// style1.setFillPattern(CellStyle.SOLID_FOREGROUND);
				/*
				 * XSSFCellStyle my_style = ExcelWBook.createCellStyle();
				 * 
				 * my_style.setFillForegroundColor(IndexedColors.BLUE.getIndex());
				 * my_style.setFillBackgroundColor(IndexedColors.RED.getIndex());
				 * Cell.setCellStyle(my_style);
				 */

			} else {
				System.out.println("Excel result valueeee is null:::::" + Result);
				Cell.setCellValue(Result);

				/*
				 * XSSFCellStyle style1 = ExcelWBook.createCellStyle();
				 * style1.setFillForegroundColor(new XSSFColor(new java.awt.Color(128, 0,
				 * 128))); Cell.setCellStyle(style1);
				 */

			}

			// Constant variables Test Data path and Test Data file name

			fileOut = new FileOutputStream(path);

			ExcelWBook.write(fileOut);

			fileOut.flush();

			fileOut.close();

		} catch (Exception ew) {

			throw (ew);

		}

	}

	public static int getRowContains(String sTestCaseName, int colNum) throws Exception {

		int i;

		try {

			int rowCount = ExcelUtilities.getRowUsed();

			for (i = 1; i < rowCount; i++) {

				if (ExcelUtilities.getCellData(i, colNum).equalsIgnoreCase(sTestCaseName)) {

					break;

				}

			}

			return i;

		} catch (Exception e) {

			// Log.error("Class ExcelUtil | Method getRowContains | Exception desc : " +
			// e.getMessage());

			throw (e);

		}

	}

	/*
	 * public static int getRowContainsBySheetName(String path,String
	 * SheetName,String sTestCaseName, int colNum) throws Exception{
	 * 
	 * int i;
	 * 
	 * 
	 * 
	 * try {
	 * 
	 * 
	 * //ExcelUtilities.setExcelFile(path, SheetName); int rowCount =
	 * ExcelUtilities.getRowUsed();
	 * 
	 * for ( i=0 ; i<rowCount; i++){
	 * 
	 * if (ExcelUtilities.getCellData(i,colNum).equalsIgnoreCase(sTestCaseName)){
	 * 
	 * break;
	 * 
	 * }
	 * 
	 * }
	 * 
	 * return i;
	 * 
	 * 
	 * 
	 * }catch (Exception e){
	 * 
	 * //Log.error("Class ExcelUtil | Method getRowContains | Exception desc : " +
	 * e.getMessage());
	 * 
	 * throw(e);
	 * 
	 * }
	 * 
	 * }
	 */

	public static int getRowContainsBySheetName(String path, String SheetName, String sTestCaseName, int colNum)
			throws Exception {

		int i;

		try {

			ExcelUtilities.setExcelFile(path, SheetName);
			int rowCount = ExcelUtilities.getRowUsed();

			for (i = 1; i < rowCount; i++) {

				if (ExcelUtilities.getCellData(i, colNum).equalsIgnoreCase(sTestCaseName)) {

					break;

				}

			}

			return i;

		} catch (Exception e) {
			// Log.error("Class ExcelUtil | Method getRowContains | Exception desc : " +
			// e.getMessage());

			throw (e);
		}

	}

	public static int getRowUsed() throws Exception {

		try {

			int RowCount = ExcelWSheet.getLastRowNum();

			// Log.info("Total number of Row used return as &lt; " + RowCount + " &gt;.");

			return RowCount;

		} catch (Exception e) {

			// Log.error("Class ExcelUtil | Method getRowUsed | Exception desc :
			// "+e.getMessage());

			System.out.println(e.getMessage());

			throw (e);

		}

	}

	public static int getUsedPhysicalNumberOfRows(String path, String sheetName) throws Exception {

		try {
			FileInputStream ExcelFile = new FileInputStream(path);
			ExcelWBook = new XSSFWorkbook(ExcelFile);
			ExcelWSheet = ExcelWBook.getSheet(sheetName);
			int RowCount = ExcelWSheet.getPhysicalNumberOfRows();

			// Log.info("Total number of Row used return as &lt; " + RowCount + " &gt;.");

			return RowCount;

		} catch (Exception e) {

			// Log.error("Class ExcelUtil | Method getRowUsed | Exception desc :
			// "+e.getMessage());

			System.out.println(e.getMessage());

			throw (e);

		}

	}

	public static void updateTestOutputsLoc(String testOutputs, int rowNum) {
		try {
			ExcelUtilities.setCellData(testOutputs, rowNum, Constants.Col_outputsPath);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			// e.printStackTrace();
		}
	}

	public static void rowsCreated(String path, String SheetName, int rows) {
		try {
			FileInputStream ExcelFile = new FileInputStream(path);
			ExcelWBook = new XSSFWorkbook(ExcelFile);
			ExcelWSheet = ExcelWBook.getSheet(SheetName);
			for (int i = 0; i <= rows; i++) {
				Row = ExcelWSheet.createRow(i);
				Cell = Row.createCell(0);
				Cell.setCellValue(1 + 1);
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
			// e.printStackTrace();
		}
	}

	public static void newRow(String path, String SheetName, int colNumber, String Value) throws Exception {
		try {

			// Open the Excel file

			FileInputStream ExcelFile = new FileInputStream(path);

			// Access the required test data sheet

			ExcelWBook = new XSSFWorkbook(ExcelFile);

			ExcelWSheet = ExcelWBook.getSheet(SheetName);

			int usedRows = ExcelWSheet.getPhysicalNumberOfRows();
			try {
				if (Row == null) {
					Row = ExcelWSheet.createRow(usedRows + 1);

					if (Cell == null) {
						Cell = Row.createCell(colNumber);
						Cell.setCellValue(Value);

					} else {
						Cell.setCellValue(Value);
					}

				}

			}

			// Log.info("Excel sheet opened");

			catch (Exception e) {

				e.printStackTrace();

			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public static void totalSubmittedCount(String ExcelFile, String SheetName, int colNum) throws Exception {
		ExcelWBook = new XSSFWorkbook(ExcelFile);

		ExcelWSheet = ExcelWBook.getSheet(SheetName);
		formatter = new DataFormatter();
		int usedRows = ExcelWSheet.getPhysicalNumberOfRows();
		System.err.println("Used rows:::::" + usedRows);

		for (int i = 1; i < usedRows; i++) {
			// String entityData =
			// ExcelWSheet.getRow(i).getCell(colNum).getStringCellValue();

			// Row = ExcelWSheet.getRow(i);
			// for(int c=3; c<=15; c++){

			Row = ExcelWSheet.getRow(i);
			int rowNum = Row.getRowNum();
			// int currentrow =
			// ExcelUtilities.getRowContainsBySheetName(Constants.File_TestCases,SheetName,
			// entityData, Constants.Col_testcaseName);
			// Cell = ExcelWSheet.getRow(currentrow).getCell(3);
			// Cell = ExcelWSheet.getRow(currentrow).getCell(4);
			// System.out.println("dfdfgdfd jdfhjf jf ::::"+Cell.getStringCellValue());
			// System.out.println("cell vakuegggg::::"+Cell);

			// String CellData1 = Row.getCell(3).getStringCellValue();
			// System.out.println("First cell data::::"+CellData1);

			String CellData1 = formatter.formatCellValue(ExcelWSheet.getRow(i).getCell(3));
			String CellData2 = formatter.formatCellValue(ExcelWSheet.getRow(i).getCell(4));
			String CellData3 = formatter.formatCellValue(ExcelWSheet.getRow(i).getCell(5));
			String CellData4 = formatter.formatCellValue(ExcelWSheet.getRow(i).getCell(6));
			String CellData5 = formatter.formatCellValue(ExcelWSheet.getRow(i).getCell(7));
			String CellData6 = formatter.formatCellValue(ExcelWSheet.getRow(i).getCell(8));
			String CellData7 = formatter.formatCellValue(ExcelWSheet.getRow(i).getCell(9));
			String CellData8 = formatter.formatCellValue(ExcelWSheet.getRow(i).getCell(10));
			String CellData9 = formatter.formatCellValue(ExcelWSheet.getRow(i).getCell(11));

			/*
			 * String CellData2 = Row.getCell(4).getStringCellValue(); String CellData3 =
			 * Row.getCell(5).getStringCellValue(); String CellData4 =
			 * Row.getCell(6).getStringCellValue(); String CellData5 =
			 * Row.getCell(7).getStringCellValue(); String CellData6 =
			 * Row.getCell(8).getStringCellValue(); String CellData7 =
			 * Row.getCell(9).getStringCellValue(); String CellData8 =
			 * Row.getCell(10).getStringCellValue(); String CellData9 =
			 * Row.getCell(11).getStringCellValue();
			 */

			int pen_submitted = Integer.parseInt(CellData1);
			int pen_Ack = Integer.parseInt(CellData2);
			int pen_Rej = Integer.parseInt(CellData3);
			int USD_Submitted = Integer.parseInt(CellData4);
			int USD_Ack = Integer.parseInt(CellData5);
			int USD_Rej = Integer.parseInt(CellData6);
			int submitted_total = Integer.parseInt(CellData7);
			int ack_total = Integer.parseInt(CellData8);
			int rej_total = Integer.parseInt(CellData9);

			int total_submitted = pen_submitted + USD_Submitted;
			int total_acks = pen_Ack + USD_Ack;
			int total_rejs = pen_Rej + USD_Rej;

			if (total_submitted == submitted_total && total_acks == ack_total && total_rejs == rej_total) {
				ExcelUtilities.setCellDataBySheetName(Constants.File_TestCases, SheetName, rowNum, Constants.status,
						"Pass");
			} else {
				ExcelUtilities.setCellDataBySheetName(Constants.File_TestCases, SheetName, rowNum, Constants.status,
						"Fail");
			}

			// }
			// Cell = Row.getCell(colNum)
		}

	}

	public static void compareTwoColomnData(String ExcelFile, String SheetName, int colNum1, int colNum2)
			throws Exception {
		ExcelWBook = new XSSFWorkbook(ExcelFile);

		ExcelWSheet = ExcelWBook.getSheet(SheetName);
		int usedRows = ExcelWSheet.getPhysicalNumberOfRows();
		for (int i = 1; i < usedRows; i++) {
			String col1 = ExcelWSheet.getRow(i).getCell(colNum1).getStringCellValue();
			String col2 = ExcelWSheet.getRow(i).getCell(colNum2).getStringCellValue();
			if (col1.equalsIgnoreCase(col2)) {
				ExcelUtilities.setCellDataBySheetName(Constants.File_TestCases, SheetName, i, Constants.view_status,
						"Pass");
			} else {
				ExcelUtilities.setCellDataBySheetName(Constants.File_TestCases, SheetName, i, Constants.view_status,
						"Fail");
			}
		}

	}

}
