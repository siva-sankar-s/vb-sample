package com.bnym.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class WinSCPFIleUplaod {
	
	//Note : Place PPK file under TestData folder in the framework
	
	//Update below data in GlobalData.properties file

	
	// Local files will be kept under \TestData\Samples folder
	
	// Example : 
	//uploadFile("Inbound\\StatementRecon\\MT103Outgoing.txt","/home/ec2-user/VolPay/apache-tomcat/VPH_messages/Input/SWIFTCHANNELIN1/" );
	
	

	public static void uploadFile(String sourceLocalFilepath, String destinationServerFilePath) throws JSchException, SftpException, IOException, InterruptedException {
		Properties prop = readPropertiesFile(System.getProperty("user.dir")+"\\TestData\\GlobalData.properties");
		String home = "/home/ec2-user/VolPay/apache-tomcat/VPH_messages/Input/";
		JSch jsch=new JSch();
		jsch.addIdentity(System.getProperty("user.dir")+"\\TestData\\GS_DEV_PrivateKey.ppk");
		Session session=jsch.getSession(prop.getProperty("userName"), prop.getProperty("host"), Integer.parseInt(prop.getProperty("port")));
		session.setConfig("PreferredAuthentications", "publickey");
		session.setConfig("StrictHostKeyChecking", "no");
		session.setConfig("UseDNS", "no");
		session.connect();
		Channel channel=session.openChannel("shell");
		channel.setInputStream(System.in);
		channel.setOutputStream(System.out);
		channel.connect(3*1000);
		ChannelSftp sftp = (ChannelSftp) session.openChannel("sftp");
		sftp.connect(3*1000);
		sftp.cd(home);
		File localFile = new File( System.getProperty("user.dir")+"/"+sourceLocalFilepath);
		//File localFile = new File(sourceLocalFilepath);
		System.out.println("localFile "+localFile);
		InputStream inputStream = new FileInputStream(localFile); 
		File f = new File(localFile.getName());
		sftp.put(inputStream,home+f.getName());
		sftp.rename(home+f.getName(), home+destinationServerFilePath+"/"+f.getName());
		sftp.disconnect();
	    sftp.exit();
	}
	
	   public static Properties readPropertiesFile(String fileName) throws IOException {
		      FileInputStream fis = null;
		      Properties prop = null;
		      try {
		         fis = new FileInputStream(fileName);
		         prop = new Properties();
		         prop.load(fis);
		      } catch(FileNotFoundException fnfe) {
		         fnfe.printStackTrace();
		      } catch(IOException ioe) {
		         ioe.printStackTrace();
		      } finally {
		         fis.close();
		      }
		      return prop;
		   }

}
