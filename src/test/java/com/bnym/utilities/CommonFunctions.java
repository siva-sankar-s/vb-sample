package com.bnym.utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.bnym.pages.AllPaymentsPage;
import com.bnym.pages.RecInspage;
import com.bnym.pages.StatementModulePage;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

/**
 * Class Description : Business Common Functions
 *
 * @author ManishThakare
 *
 */
public class CommonFunctions extends LoginLogout {
	public String insId = "";
	public static String outputInstructionId= null;

	/**
	 * Method Name: performAdvancedSearchTest
	 * 
	 * Description: Perform Advanced Search Test
	 * 
	 * @param sheetName
	 * @param TestCaseName
	 * @throws Exception
	 */
	public void performAdvancedSearchTest(String sheetName, String TestCaseName, WebDriver driver) throws Exception {
		String sPSA = "";
		String sMOP = "";
		String sStatus = "";
		String sValue1 = "";
		String sValue2 = "";
		String sInstruction_Id = "";
		String replacedInstructionId = "";
		boolean isEmpty = false;

		try {
			log = Logger.getLogger(TestCaseName);
			test = report.startTest(TestCaseName);
			sInstruction_Id = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, sheetName, TestCaseName,
					Constants.Col_InstructionId);
			replacedInstructionId = sInstruction_Id.replaceAll("ID-", "");

			sPSA = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, sheetName, TestCaseName,
					Constants.Col_SearchPSA);
			sMOP = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, sheetName, TestCaseName,
					Constants.Col_MOP);

			sStatus = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, sheetName, TestCaseName,
					Constants.Col_PaymentStatus);

			log.info(replacedInstructionId);

			js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", AllPaymentsPage.showAdvancedSearchButton(driver));
			Thread.sleep(5000);

			WebElement instructionIdSearchBox = AllPaymentsPage.instructionIdSearchBox(driver);
			instructionIdSearchBox.sendKeys(sInstruction_Id, Keys.ENTER);

			WebElement sPsaCode = AllPaymentsPage.partyServiceAssociationCodeSearchBox(driver);
			sPsaCode.sendKeys(sPSA);
			Thread.sleep(5000);
			sPsaCode.sendKeys(Keys.ENTER);

			WebElement sMethodOfPayment = AllPaymentsPage.methodOfPaymentSearchBox(driver);
			sMethodOfPayment.sendKeys(sMOP, Keys.ENTER);

			WebElement sPaymentStatus = AllPaymentsPage.paymentStatusSearchBox(driver);
			sPaymentStatus.sendKeys(sStatus, Keys.ENTER);

			String sPreviousDate = GenericFunctions.getPreviousCalendarDate(-1);
			log.info(sPreviousDate);

			WebElement sEndDate = AllPaymentsPage.receivedDateEndTextField(driver);
			sEndDate.sendKeys(sPreviousDate, Keys.ENTER);

			for (int i = -1; i >= -5; i--) {

				WebElement sStartDate = AllPaymentsPage.receivedDateStartTextField(driver);
				sStartDate.clear();
				sStartDate.sendKeys(GenericFunctions.getPreviousCalendarDate(i), Keys.ENTER);

				AllPaymentsPage.searchButton(driver).sendKeys(Keys.ENTER);
				Thread.sleep(20000);

				String noRecordInTable = AllPaymentsPage.noSearchRecord(driver).getText();

				if (!(noRecordInTable.equals("0 records shown"))) {
					WebElement sOriginalMsgFunValue = AllPaymentsPage.sOriginalMassageSubFunctionCol(driver);
					WebElement sPStatus = AllPaymentsPage.paymentStatusCol(driver);
					sValue1 = sPStatus.getText();

					sValue2 = sOriginalMsgFunValue.getText();
					isEmpty = sValue2.isEmpty();
					WebElement sInstructionId = AllPaymentsPage.instructionIDInSearchedTable(driver);
					insId = WaitLibrary.waitForElementToBeVisible(driver, sInstructionId, 60).getText();
					System.out.println("Instruction id::" + insId);

					if (sValue1.equals(sStatus) && isEmpty == true && replacedInstructionId.equals(insId)) {
						Thread.sleep(5000);
						AllPaymentsPage.sPSACodeSearchedResult(LoginLogout.driver).click();

					}
				} else if (noRecordInTable.equals("0 records shown")) {
					log.info("Search Continues For Previous Dates");
					Thread.sleep(8000);
					WaitLibrary
							.waitForElementToBeClickable(driver, AllPaymentsPage.showAdvancedSearchButton(driver), 300)
							.click();
					Thread.sleep(5000);
				}

			}
		} catch (Exception e) {
			throw new Exception("Error Occured In performAdvancedSearchTest():" + e.getMessage());
		}
	}

	/**
	 * Method Name: performAdvancedSearchTest
	 * 
	 * Description: Perform Advanced Search Test
	 * 
	 * @param sheetName
	 * @param TestCaseName
	 * @throws Exception
	 */
	public void performAdvancedSearchWithInstructionIdTest(String sheetName, String TestCaseName, WebDriver driver)
			throws Exception {
		String sStatus = "";
		String sValue1 = "";
		String sValue2 = "";
		String sInstruction_Id = "";
		String replacedInstructionId = "";
		boolean isEmpty = false;

		try {
			log = Logger.getLogger(TestCaseName);
			test = report.startTest(TestCaseName);
			sInstruction_Id = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, sheetName, TestCaseName,
					Constants.Col_InstructionId);
			replacedInstructionId = sInstruction_Id.replaceAll("ID-", "");
			sStatus = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, sheetName, TestCaseName,
					Constants.Col_PaymentStatus);

			log.info(replacedInstructionId);
			((ExtentTest) test).log(LogStatus.PASS, "Instruction ID Is :" + replacedInstructionId);
			js = (JavascriptExecutor) driver;
			Thread.sleep(5000);
			js.executeScript("arguments[0].click();", AllPaymentsPage.showAdvancedSearchButton(driver));
			((ExtentTest) test).log(LogStatus.PASS, "Clicked On Show Advanced Search Button");
			Thread.sleep(5000);

			WebElement instructionIdSearchBox = AllPaymentsPage.instructionIdSearchBox(driver);
			instructionIdSearchBox.sendKeys(sInstruction_Id, Keys.ENTER);
			((ExtentTest) test).log(LogStatus.PASS, "Entered Instrction ID into Instruction ID SearcBox");
			Thread.sleep(5000);

			AllPaymentsPage.searchButton(driver).sendKeys(Keys.ENTER);
			((ExtentTest) test).log(LogStatus.PASS, "Clicked On Search Button");
			Thread.sleep(20000);

			String noRecordInTable = AllPaymentsPage.noSearchRecord(driver).getText();

			if (!(noRecordInTable.equals("0 records shown"))) {
				WebElement sOriginalMsgFunValue = AllPaymentsPage.sOriginalMassageSubFunctionCol(driver);
				WebElement sPStatus = AllPaymentsPage.paymentStatusCol(driver);
				sValue1 = sPStatus.getText();

				sValue2 = sOriginalMsgFunValue.getText();
				isEmpty = sValue2.isEmpty();
				WebElement sInstructionId = AllPaymentsPage.instructionIDInSearchedTable(driver);
				insId = WaitLibrary.waitForElementToBeVisible(driver, sInstructionId, 60).getText();
				System.out.println("Instruction id::" + insId);

				if (sValue1.equals(sStatus) && isEmpty == true && replacedInstructionId.equals(insId)) {
					Thread.sleep(8000);
					js.executeScript("arguments[0].click();", AllPaymentsPage.sPSACodeSearchedResult(driver));
					((ExtentTest) test).log(LogStatus.PASS, "Clicked On Found Payment");

				}
			}

		} catch (Exception e) {
			throw new Exception("Error Occured In performAdvancedSearchTest():" + e.getMessage());
		}
	}

	public static void advancedSerchWithPaymentReference(WebDriver driver, String paymentReference)
			throws InterruptedException {
		Thread.sleep(3000);
		Utils.clickByJs(AllPaymentsPage.getShowAdvanceSearchButton(driver));
		// driver.findElement(ByAngular.buttonText("Show Advanced Search")).click();
		Thread.sleep(1000);
		String del = Keys.chord(Keys.CONTROL, "a") + Keys.DELETE;
		AllPaymentsPage.getPaymentRefTextBox(driver).sendKeys(del);
		Thread.sleep(1000);
		AllPaymentsPage.getPaymentRefTextBox(driver).sendKeys(paymentReference);
		Thread.sleep(1000);
		Utils.clickByJs(AllPaymentsPage.searchButton(driver));

	}

	/**
	 * Method Name: selectReceivedStatement
	 * 
	 * Description: selectReceivedStatement
	 * 
	 * @param sAttributeName
	 * @throws Exception
	 */
	public static void selectReceivedStatement(String sAttributeName) throws Exception {
		try {
			if (sAttributeName.equals("fa fa-minus")) {
				Thread.sleep(3000);

				WaitLibrary.waitForElementToBeClickable(driver, AllPaymentsPage.collapsePaymentsModule(driver), 55)
						.click();

			}
			Thread.sleep(2000);
			WaitLibrary.waitForElementToBeClickable(driver, StatementModulePage.statementModule(driver), 300).click();
			Thread.sleep(2000);
			WaitLibrary.waitForElementToBeClickable(driver, StatementModulePage.receivedStatements(driver), 300)
					.click();
			Thread.sleep(8000);
			WaitLibrary.waitForElementToBeClickable(driver, StatementModulePage.btnListView(driver), 300).click();
			Thread.sleep(50000);

			WebElement sStatement = StatementModulePage.selectStatementByName(driver,
					"StatementMatching_Outbound_1000CTR.txt");
			WaitLibrary.waitForElementToBeClickable(driver, sStatement, 300).click();
		} catch (Exception e) {
			throw new Exception("Error Occcured In Selecting Received Staement" + e.getMessage());
		}
	}

//------------------------------------Deepak-----------------------------------------------------------------------------------

	public void performAdvancedSearchTest1(String sheetName, String TestCaseName, WebDriver driver) throws Exception {

		String sStatus = "";
		String pRefrence = "";
		String sValue1 = "";
		String sValue2 = "";
		String sInstruction_Id = "";
		String replacedInstructionId = "";
		boolean isEmpty = false;

		try {
			log = Logger.getLogger(TestCaseName);

			sStatus = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, sheetName, TestCaseName,
					Constants.Col_PaymentStatus);

			pRefrence = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, sheetName, TestCaseName,
					Constants.Col_pRefrence);

			log.info(sStatus + "Payment Refrence no is ::::" + pRefrence);

			js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", AllPaymentsPage.showAdvancedSearchButton(driver));
			Thread.sleep(5000);

			WebElement paymentEltemID = AllPaymentsPage.paymentEltemBox(driver);
			paymentEltemID.sendKeys(pRefrence, Keys.ENTER);

			Thread.sleep(1000);
			AllPaymentsPage.searchButton(driver).sendKeys(Keys.ENTER);
			Thread.sleep(20000);

			js.executeScript("arguments[0].click();", RecInspage.advancedSerachedResultPaymentId(driver));
			Thread.sleep(3000);

			String noRecordInTable = AllPaymentsPage.noSearchRecord(driver).getText();

			if (!(noRecordInTable.equals("0 records shown"))) {
				WebElement sOriginalMsgFunValue = AllPaymentsPage.sOriginalMassageSubFunctionCol(driver);
				WebElement sPStatus = AllPaymentsPage.paymentStatusCol(driver);
				sValue1 = sPStatus.getText();

				sValue2 = sOriginalMsgFunValue.getText();
				isEmpty = sValue2.isEmpty();
				WebElement sInstructionId = AllPaymentsPage.instructionIDInSearchedTable(driver);
				insId = WaitLibrary.waitForElementToBeVisible(driver, sInstructionId, 60).getText();
				System.out.println("Instruction id::" + insId);

				if (sValue1.equals(sStatus) && isEmpty == true && replacedInstructionId.equals(insId)) {
					Thread.sleep(5000);
					AllPaymentsPage.sPSACodeSearchedResult(LoginLogout.driver).click();

				}

			} else if (noRecordInTable.equals("0 records shown")) {
				log.info("Search Continues For Previous Dates");
				Thread.sleep(8000);
				WaitLibrary.waitForElementToBeClickable(driver, AllPaymentsPage.showAdvancedSearchButton(driver), 300)
						.click();
				Thread.sleep(5000);
			}

		}

		catch (Exception e) {
			throw new Exception("Error Occured In performAdvancedSearchTest():" + e.getMessage());
		}
	}

//public static void advancedSerchWithPaymentReference(WebDriver driver, String paymentReference) throws InterruptedException {
//    JavascriptExecutor jsEx = ((JavascriptExecutor)driver);
//    Thread.sleep(5000);
//    //Utils.clickByJs(AllPaymentsPage.getShowAdvanceSearchButton(driver));
//    jsEx.executeScript("arguments[0].click();",AllPaymentsPage.getShowAdvanceSearchButton(driver));
//    Thread.sleep(3000);
//    //AllPaymentsPage.getPaymentRefTextBox(driver).sendKeys(paymentReference);
//    //Thread.sleep(3000);
//    jsEx.executeScript("arguments[0].click();",AllPaymentsPage.searchButton(driver));
//    Thread.sleep(3000);
//}

	public static void updateFedNackFile1(String sourcefilePath, String destfilePath, String outputInstructionId) {
		try {
			System.out.println("sourcefilePath statement::::::::::" + sourcefilePath);
			System.out.println("destfilePath statement::::::::::" + destfilePath);
			String currentLine = "";
			String oldContent = "";
			String modifiedValueData = "";
			File file = new File(sourcefilePath);
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);

			while ((currentLine = br.readLine()) != null) {
				oldContent = oldContent + currentLine + System.lineSeparator();
			}
			System.out.println("oldContent::::::::" + oldContent);
			if (outputInstructionId != null) {
				System.out.println("paydetails[2]::::::::::::::::::::::::" + outputInstructionId);
				modifiedValueData = oldContent.replace("OUTPUT_INSTRUCTIONN_ID", outputInstructionId);
				System.out
						.println("modifiedValueData1::::::::::::::::::::::::::::::::::::::::::::" + modifiedValueData);

			}
			String finaldata = modifiedValueData;
			int iFileLength = finaldata.length() - 2;
			FileWriter fw = new FileWriter(destfilePath);
			br.close();
			fw.write(finaldata, 0, iFileLength);
			fw.flush();
			fw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


public static void verifyDestinationMessageType(String sMessageType, WebDriver driver) throws Exception {
    try {
        WebElement MsgType = AllPaymentsPage.destinationMessageType1(driver);
        String sMsgType = MsgType.getText();
        System.out.println(sMsgType);
        boolean isTrue = sMsgType.contains(sMessageType);
        System.out.println(isTrue);
        Log.assertVerify(true, sMsgType.contains(sMessageType), sMsgType);
        ((ExtentTest) test).log(LogStatus.PASS, "Destination Message Type Is :" + sMsgType);
    } catch (Exception e) {
        throw new Exception("Error Occured In Verify Destination Message Type" + e.getMessage());
    }
}




//--------------------------------------------------------Deepak End----------------------------------------------------

	
	/**
     * Method Name: performAdvancedSearchTest
     * 
     * Description: Perform Advanced Search Test
     * 
     * @param sheetName
     * @param TestCaseName
     * @throws Exception
     */
    public void performAdvancedSearchWithInstructionIdTest1(String sheetName, String TestCaseName, WebDriver driver)
            throws Exception {
        String sStatus = "";
        String sValue1 = "";
        String sValue2 = "";
        String imad = "";
        String replacedInstructionId = "";
        boolean isEmpty = false;

 

        try {
            log = Logger.getLogger(TestCaseName);

 

			imad = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData,"GSStatements", TestCaseName,
					Constants.Col_IMAD);
            sStatus = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, sheetName, TestCaseName,
                    Constants.Col_PaymentStatus);
            replacedInstructionId = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, sheetName, TestCaseName,
                    Constants.Col_InstructionId);
 
            replacedInstructionId = replacedInstructionId.replaceAll("ID-", "");
            log.info(replacedInstructionId);
            CommonFunctions.advancedSerchWithPaymentReference(driver,imad);
            Thread.sleep(2000);
            String noRecordInTable = AllPaymentsPage.noSearchRecord(driver).getText();

 

            if (!(noRecordInTable.equals("0 records shown"))) {
                WebElement sOriginalMsgFunValue = AllPaymentsPage.sOriginalMassageSubFunctionCol(driver);
                WebElement sPStatus = AllPaymentsPage.paymentStatusCol(driver);
                sValue1 = sPStatus.getText();

                sValue2 = sOriginalMsgFunValue.getText();
                isEmpty = sValue2.isEmpty();
                WebElement sInstructionId = AllPaymentsPage.instructionIDInSearchedTable(driver);
                Thread.sleep(2000);
               insId = WaitLibrary.waitForElementToBeVisible(driver, sInstructionId, 60).getText();
                System.out.println("Instruction id:" + insId);

                Thread.sleep(1000);
                if (sValue1.equals(sStatus) && isEmpty == true && replacedInstructionId.equals(insId)) {
                    Thread.sleep(5000);
        			LoginLogout.js.executeScript("arguments[0].click();", RecInspage.advancedSerachedResultPaymentId(driver));
        			Thread.sleep(2000);
                }
            }

 

        } catch (Exception e) {
            throw new Exception("Error Occured In performAdvancedSearchTest():" + e.getMessage());
        }
    }

}
