package com.bnym.utilities;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;

public class FileUtilities {
	// static String path= "/BNYM_RTP_Automation/workspace/BNYM/All_Outputs";
	
	public static void updateReportResponseFile(String sourcefilePath, String destfilePath, String messageID, int stringLenOfFile)
	{
		try{
			System.out.println("sourcefilePath statement::::::::::"+sourcefilePath);
			System.out.println("destfilePath statement::::::::::"+destfilePath);
			String currentLine="";
			String oldContent="";
			String modifiedValueData = "";
			File file =new File(sourcefilePath);
			FileReader fr =new FileReader(file);
			BufferedReader br =new BufferedReader(fr);

			while((currentLine=br.readLine())!=null)
			{
				oldContent=oldContent+currentLine+System.lineSeparator();
			}
			System.out.println("oldContent::::::::"+oldContent);
			if(messageID != null)
			{
				System.out.println("paydetails[2]::::::::::::::::::::::::"+messageID);
				modifiedValueData = oldContent.replace("MESAGEID", messageID);
				System.out.println("modifiedValueData1::::::::::::::::::::::::::::::::::::::::::::"+modifiedValueData);
			}
//			String finaldata = modifiedValueData.trim();
			String finaldata = modifiedValueData;
			FileWriter fw =new FileWriter(destfilePath);
			br.close();
			fw.write(finaldata, 0, stringLenOfFile);
			fw.flush();
			fw.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void updateFedRRFIFile_1500CTRBTR(String sourcefilePath, String destfilePath, String outputInstructionId)
	{
		try{
			System.out.println("sourcefilePath statement::::::::::"+sourcefilePath);
			System.out.println("destfilePath statement::::::::::"+destfilePath);
			String currentLine="";
			String oldContent="";
			String modifiedValueData = "";
			File file =new File(sourcefilePath);
			FileReader fr =new FileReader(file);
			BufferedReader br =new BufferedReader(fr);

			while((currentLine=br.readLine())!=null)
			{
				oldContent=oldContent+currentLine+System.lineSeparator();
			}
			System.out.println("oldContent::::::::"+oldContent);
			if(outputInstructionId != null)
			{
				System.out.println("paydetails[2]::::::::::::::::::::::::"+outputInstructionId);
				modifiedValueData = oldContent.replace("OUTPUT_INSTRUCTIONN_ID", outputInstructionId);
				System.out.println("modifiedValueData1::::::::::::::::::::::::::::::::::::::::::::"+modifiedValueData);
			}
//			String finaldata = modifiedValueData.trim();
			String finaldata = modifiedValueData;
			FileWriter fw =new FileWriter(destfilePath);
			br.close();
			fw.write(finaldata,0,376);
			fw.flush();
			fw.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void vkUpdateFedRROFFile(String sourcefilePath, String destfilePath, String outputInstructionId)
	{
		try{
			System.out.println("sourcefilePath statement::::::::::"+sourcefilePath);
			System.out.println("destfilePath statement::::::::::"+destfilePath);
			String currentLine="";
			String oldContent="";
			String modifiedValueData = "";
			File file =new File(sourcefilePath);
			FileReader fr =new FileReader(file);
			BufferedReader br =new BufferedReader(fr);

			while((currentLine=br.readLine())!=null)
			{
				oldContent=oldContent+currentLine+System.lineSeparator();
			}
			System.out.println("oldContent::::::::"+oldContent);
			if(outputInstructionId != null)
			{
				System.out.println("paydetails[2]::::::::::::::::::::::::"+outputInstructionId);
				modifiedValueData = oldContent.replace("OUTPUT_INSTRUCTIONN_ID", outputInstructionId);
				System.out.println("modifiedValueData1::::::::::::::::::::::::::::::::::::::::::::"+modifiedValueData);
			}
//			String finaldata = modifiedValueData.trim();
			String finaldata = modifiedValueData;
			FileWriter fw =new FileWriter(destfilePath);
			br.close();
			fw.write(finaldata,0,384);
			fw.flush();
			fw.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void vkUpdateFedRRFIFile(String sourcefilePath, String destfilePath, String outputInstructionId)
	{
		try{
			System.out.println("sourcefilePath statement::::::::::"+sourcefilePath);
			System.out.println("destfilePath statement::::::::::"+destfilePath);
			String currentLine="";
			String oldContent="";
			String modifiedValueData = "";
			File file =new File(sourcefilePath);
			FileReader fr =new FileReader(file);
			BufferedReader br =new BufferedReader(fr);

			while((currentLine=br.readLine())!=null)
			{
				oldContent=oldContent+currentLine+System.lineSeparator();
			}
			System.out.println("oldContent::::::::"+oldContent);
			if(outputInstructionId != null)
			{
				System.out.println("paydetails[2]::::::::::::::::::::::::"+outputInstructionId);
				modifiedValueData = oldContent.replace("OUTPUT_INSTRUCTIONN_ID", outputInstructionId);
				System.out.println("modifiedValueData1::::::::::::::::::::::::::::::::::::::::::::"+modifiedValueData);
			}
//			String finaldata = modifiedValueData.trim();
			String finaldata = modifiedValueData;
			FileWriter fw =new FileWriter(destfilePath);
			br.close();
			fw.write(finaldata,0,468);
			fw.flush();
			fw.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static String createParentDir(String suiteLoc, String name) {
		File file = new File(suiteLoc + "/" + name);
		if (!file.exists()) {
			if (file.mkdir()) {
				System.out.println("Directory is created!");
			} else {
				System.out.println("Failed to create directory!");
			}
		}
		return file.getAbsolutePath();
	}

	public static String createAllOutputs(String suiteLoc) {
		File file = new File(suiteLoc + "/" + "All_OUTPUTS");
		if (!file.exists()) {
			if (file.mkdir()) {
				System.out.println("Directory is created!");
			} else {
				System.out.println("Failed to create directory!");
			}
		}
		return file.getAbsolutePath();
	}

	public static String createsubDir(String path, String folName) {

		// To create directory
		File file = new File(path + "/" + folName);
		if (!file.exists()) {
			if (file.mkdir()) {

				System.out.println(file.getName());
				System.out.println("Directory is created!");

			} else {
				System.out.println("Failed to create directory!");
			}

		}

		return file.getAbsolutePath();

	}

	public static String updatePCDDATA(String path, String value) {
		String updatedData = null;

		return updatedData;
	}

	public static void updateStatementFileForSwiftChannelIn(String sourcefilePath, String destfilePath,
			String outputInstructionId) {
		// if(paydetails != null)
		/*
		 * System.out.println("payment11111111111111111111111"+paydetails[0]);
		 * System.out.println("payment111111111111111111111112222222"+paydetails[1]);
		 * System.out.println("payment111111111111111111111113333333333"+paydetails[2]);
		 */
		try {
			System.out.println("sourcefilePath statement::::::::::" + sourcefilePath);
			System.out.println("destfilePath statement::::::::::" + destfilePath);
			String currentLine = "";
			String oldContent = "";
			// String modifiedCur="";
			String modifiedValueData = "";
			// String modifiedRef ="";
			File file = new File(sourcefilePath);
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);

			while ((currentLine = br.readLine()) != null) {
				oldContent = oldContent + currentLine + System.lineSeparator();
			}
			System.out.println("oldContent::::::::" + oldContent);
			if (outputInstructionId != null) {
				System.out.println("paydetails[2]::::::::::::::::::::::::" + outputInstructionId);
				modifiedValueData = oldContent.replace("OUTPUTINSID", outputInstructionId);
				System.out
						.println("modifiedValueData1::::::::::::::::::::::::::::::::::::::::::::" + modifiedValueData);
				Calendar calendar = Calendar.getInstance();
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMdd");
				System.out.println(dateFormat.format(calendar.getTime()));
				modifiedValueData = modifiedValueData.replace("VALUEDATE", dateFormat.format(calendar.getTime()));

				System.out
						.println("modifiedValueData2::::::::::::::::::::::::::::::::::::::::::::" + modifiedValueData);
			}
			// BufferedReader br1 = new BufferedReader(new FileReader(new File(filePath)));
			/*
			 * if(paydetails[0] != null) { System.out.println(
			 * "modifiedValueData::::::::::::::::::::::::::::::::::::::::::::"+
			 * modifiedValueData); modifiedRef = modifiedValueData.replace("valuedate",
			 * paydetails[0]); } if(modifiedCur != null) {
			 * System.out.println("modifiedRef::::::::::::::::::::::::::::::::::::::::::::"+
			 * modifiedRef); modifiedCur = modifiedRef.replace("currency", paydetails[1]); }
			 */
			String finaldata = modifiedValueData.trim();
			// String finalModifiedString = modifiedCur
			FileWriter fw = new FileWriter(destfilePath);
			// System.out.println("modifiedCurmodifiedCurmodifiedCurmodifiedCurmodifiedCur::::::::::::::"+modifiedCur);
			br.close();
			fw.write(finaldata);
			fw.flush();
			fw.close();
		}

		catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void updateFedAckFile(String sourcefilePath, String destfilePath, String outputInstructionId) {
		try {
			System.out.println("sourcefilePath statement::::::::::" + sourcefilePath);
			System.out.println("destfilePath statement::::::::::" + destfilePath);
			String currentLine = "";
			String oldContent = "";
			String modifiedValueData = "";
			File file = new File(sourcefilePath);
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);

			while ((currentLine = br.readLine()) != null) {
				oldContent = oldContent + currentLine + System.lineSeparator();
			}
			System.out.println("oldContent::::::::" + oldContent);
			if (outputInstructionId != null) {
				System.out.println("paydetails[2]::::::::::::::::::::::::" + outputInstructionId);
				modifiedValueData = oldContent.replace("DATELTERMIDINSTRUC", outputInstructionId);
				System.out
						.println("modifiedValueData1::::::::::::::::::::::::::::::::::::::::::::" + modifiedValueData);

				String sDate = Utils.getSubString(outputInstructionId, 0, 4);
				System.out.println(sDate);
				modifiedValueData = modifiedValueData.replace("LAST", sDate);
				System.out
						.println("modifiedValueData2::::::::::::::::::::::::::::::::::::::::::::" + modifiedValueData);
				Random rand = new Random();
				int value = rand.nextInt(200);
				String sValue = Integer.toString(value);
				System.out.println(sValue);
				modifiedValueData = modifiedValueData.replace("ran", sValue);
				System.out
						.println("modifiedValueData2::::::::::::::::::::::::::::::::::::::::::::" + modifiedValueData);

				Calendar calendar = Calendar.getInstance();
				SimpleDateFormat dateFormat = new SimpleDateFormat("MMdd");
				System.out.println(dateFormat.format(calendar.getTime()));
				modifiedValueData = modifiedValueData.replace("TAKE", dateFormat.format(calendar.getTime()));
				System.out
						.println("modifiedValueData2::::::::::::::::::::::::::::::::::::::::::::" + modifiedValueData);

			}
			String finaldata = modifiedValueData;
			int iFileLength = finaldata.length() - 2;
			FileWriter fw = new FileWriter(destfilePath);
			br.close();
			fw.write(finaldata, 0, iFileLength);
			fw.flush();
			fw.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void vk_UpdateFedAckFile(String sourcefilePath, String destfilePath, String outputInstructionId)
	{
		try{
			System.out.println("sourcefilePath statement::::::::::"+sourcefilePath);
			System.out.println("destfilePath statement::::::::::"+destfilePath);
			String currentLine="";
			String oldContent="";
			String modifiedValueData = "";
			File file =new File(sourcefilePath);
			FileReader fr =new FileReader(file);
			BufferedReader br =new BufferedReader(fr);

			while((currentLine=br.readLine())!=null)
			{
				oldContent=oldContent+currentLine+System.lineSeparator();
			}
			System.out.println("oldContent::::::::"+oldContent);
			if(outputInstructionId != null)
			{
				System.out.println("paydetails[2]::::::::::::::::::::::::"+outputInstructionId);
				modifiedValueData = oldContent.replace("DATELTERMIDINSTRUC", outputInstructionId);
				System.out.println("modifiedValueData1::::::::::::::::::::::::::::::::::::::::::::"+modifiedValueData);
				Calendar calendar = Calendar.getInstance();
				 SimpleDateFormat dateFormat = new SimpleDateFormat("MMdd");
				 System.out.println(dateFormat.format(calendar.getTime()));
				 modifiedValueData = modifiedValueData.replace("LAST", dateFormat.format(calendar.getTime()));
				System.out.println("modifiedValueData2::::::::::::::::::::::::::::::::::::::::::::"+modifiedValueData);
				modifiedValueData = modifiedValueData.replace("Last24Spaces------------", "                        ");
			}
//			String finaldata = modifiedValueData.trim();
			String finaldata = modifiedValueData;
			FileWriter fw =new FileWriter(destfilePath);
			br.close();
			fw.write(finaldata, 0, 125);
			fw.flush();
			fw.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void updateFedErrorCodeIncomingNackFile(String sourcefilePath, String destfilePath, String outputInstructionId) {
		try {
			System.out.println("sourcefilePath statement::::::::::" + sourcefilePath);
			System.out.println("destfilePath statement::::::::::" + destfilePath);
			String currentLine = "";
			String oldContent = "";
			String modifiedValueData = "";
			File file = new File(sourcefilePath);
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);

			while ((currentLine = br.readLine()) != null) {
				oldContent = oldContent + currentLine + System.lineSeparator();
			}
			System.out.println("oldContent::::::::" + oldContent);
			if (outputInstructionId != null) {
				System.out.println("paydetails[2]::::::::::::::::::::::::" + outputInstructionId);
				modifiedValueData = oldContent.replace("OUTPUT_INSTRUCTIONN_ID", outputInstructionId);
				System.out.println("modifiedValueData1::::::::::::::::::::::::::::::::::::::::::::" + modifiedValueData);

				Calendar calendar = Calendar.getInstance();
				SimpleDateFormat dateFormat = new SimpleDateFormat("MMdd");
				System.out.println(dateFormat.format(calendar.getTime()));
				modifiedValueData = modifiedValueData.replace("DATE", dateFormat.format(calendar.getTime()));
				System.out.println("modifiedValueData2::::::::::::::::::::::::::::::::::::::::::::" + modifiedValueData);

			}
			String finaldata = modifiedValueData;
			int iFileLength = finaldata.length() - 2;
			FileWriter fw = new FileWriter(destfilePath);
			br.close();
			fw.write(finaldata, 0, iFileLength);
			fw.flush();
			fw.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method Name:
	 * 
	 * Description:
	 * 
	 * @param sourcefilePath
	 * @param destfilePath
	 * @param outputInstructionId
	 */
	public static void updateFedNackFile(String sourcefilePath, String destfilePath, String outputInstructionId) {
		try {
			System.out.println("sourcefilePath statement::::::::::" + sourcefilePath);
			System.out.println("destfilePath statement::::::::::" + destfilePath);
			String currentLine = "";
			String oldContent = "";
			String modifiedValueData = "";
			File file = new File(sourcefilePath);
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);

			while ((currentLine = br.readLine()) != null) {
				oldContent = oldContent + currentLine + System.lineSeparator();
			}
			System.out.println("oldContent::::::::" + oldContent);
			if (outputInstructionId != null) {
				System.out.println("paydetails[2]::::::::::::::::::::::::" + outputInstructionId);
				modifiedValueData = oldContent.replace("OUTPUT_INSTRUCTIONN_ID", outputInstructionId);
				System.out.println("modifiedValueData1::::::::::::::::::::::::::::::::::::::::::::" + modifiedValueData);

			}
			String finaldata = modifiedValueData;
			int iFileLength = finaldata.length() - 2;
			FileWriter fw = new FileWriter(destfilePath);
			br.close();
			fw.write(finaldata, 0, iFileLength);
			fw.flush();
			fw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void updateStatementFileInboundAndOutbound(String sourcefilePath, String destfilePath,
			String outputInstructionId, String trarefId) {
		// if(paydetails != null)
		/*
		 * System.out.println("payment11111111111111111111111"+paydetails[0]);
		 * System.out.println("payment111111111111111111111112222222"+paydetails[1]);
		 * System.out.println("payment111111111111111111111113333333333"+paydetails[2]);
		 */
		try {
			System.out.println("sourcefilePath statement::::::::::" + sourcefilePath);
			System.out.println("destfilePath statement::::::::::" + destfilePath);
			String currentLine = "";
			String oldContent = "";
			// String modifiedCur="";
			String modifiedValueData = "";
			String modifiedValueData1 = "";
			// String modifiedRef ="";
			File file = new File(sourcefilePath);
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);

			while ((currentLine = br.readLine()) != null) {
				oldContent = oldContent + currentLine + System.lineSeparator();
			}
			System.out.println("oldContent::::::::" + oldContent);
			if (outputInstructionId != null) {
				System.out.println("paydetails[2]::::::::::::::::::::::::" + outputInstructionId);
				modifiedValueData = oldContent.replace("OUTPUTINSID", outputInstructionId);
			}
			if (trarefId != null) {
				System.out.println("modifiedValueData::::::::::::::::::::::::::::::::::::::::::::" + modifiedValueData);
				modifiedValueData1 = modifiedValueData.replace("INPUTTRAID", trarefId);
			}

			/*
			 * if(modifiedCur != null) {
			 * System.out.println("modifiedRef::::::::::::::::::::::::::::::::::::::::::::"+
			 * modifiedRef); modifiedCur = modifiedRef.replace("currency", paydetails[1]); }
			 */
			String finaldata = modifiedValueData1.trim();
			// String finalModifiedString = modifiedCur
			FileWriter fw = new FileWriter(destfilePath);
			// System.out.println("modifiedCurmodifiedCurmodifiedCurmodifiedCurmodifiedCur::::::::::::::"+modifiedCur);
			br.close();
			fw.write(finaldata);
			fw.flush();
			fw.close();
		}

		catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static String getTransactionRefId(String filePath) {
		String transactionrefNumber = null;
		try {
			BufferedReader br1 = new BufferedReader(new FileReader(new File(filePath)));
			String line = null;

			while ((line = br1.readLine()) != null) {
				if (line.startsWith(":20:")) {
					int index = line.indexOf("20:");
					transactionrefNumber = line.substring(index + 3);
					/*
					 * if(transactionrefNumber != null) { payMentDetails = transactionrefNumber; }
					 */
				}
			}
			br1.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return transactionrefNumber;
	}

	public static void getUpdateMT199Reject(String filePath, String outputinsid) {
		String transactionrefNumber = null;
		try {
			System.out.println("filePath:" + filePath);
			BufferedReader br1 = new BufferedReader(new FileReader(new File(filePath)));

			BufferedReader br2 = new BufferedReader(new FileReader(new File(filePath)));
			BufferedWriter bw1 = null;
			String line = null;
			int totalLines = 1;
			int count = 1;
			while (br2.readLine() != null) {
				totalLines++;
			}
			while ((line = br1.readLine()) != null) {
				if (count == 1) {
					bw1 = new BufferedWriter(new FileWriter(new File(filePath)));
					bw1.write(line + "\r\n");
				} else if (line.startsWith(":21:")) {
					int index = line.indexOf("21:");
					transactionrefNumber = line.substring(index + 3);
					line = line.replace(transactionrefNumber, outputinsid);
					System.out.println("line:" + line);
					bw1.write(line + "\r\n");
				} else if (count < (totalLines - 1)) {
					bw1.write(line + "\r\n");
				}

				else {
					bw1.write(line);
				}
				count++;
			}
			br1.close();
			br2.close();
			bw1.flush();
			bw1.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// instruction id start//
	public static void SetInstructionId(String filePath, String outputInsId) {
		String transactionrefNumber = null;
		try {
			BufferedReader br2 = new BufferedReader(new FileReader(new File(filePath)));
			BufferedReader br1 = new BufferedReader(new FileReader(new File(filePath)));
			String line = null;
			int totalLines = 1;
			BufferedWriter bw = null;
			int count = 1;
			while (br2.readLine() != null) {
				totalLines++;
			}
			System.out.println("totalLines" + totalLines);

			while ((line = br1.readLine()) != null) {

				if (count == 1) {
					System.out.println("cnt 1");
					bw = new BufferedWriter(new FileWriter(new File(filePath)));
					bw.write(line + "\r\n");

				}

				else if (line.startsWith("/MREF/")) {
					int index = line.indexOf("MREF/");
					transactionrefNumber = line.substring(index + 5);
					if (transactionrefNumber.equals("") && outputInsId != null) {
						String newline = line.replaceAll(line, line + outputInsId);
						bw.write(newline + "\r\n");
					} else if (outputInsId != null) {

						line = line.replace(transactionrefNumber, outputInsId);
						bw.write(line + "\r\n");
					}
				}

				else if (count < (totalLines - 1)) {
					bw.write(line + "\r\n");
				} else {
					bw.write(line);
				}
				count++;
			}
			System.out.println("Count::::::::::::::::::" + count);
			br1.close();
			br2.close();
			bw.flush();
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// instruction id end//

	public static String getNOSAccount(String filePath) {
		String nosaccount = null;
		try {
			BufferedReader br1 = new BufferedReader(new FileReader(new File(filePath)));
			String line = null;

			while ((line = br1.readLine()) != null) {
				if (line.startsWith(":25:")) {
					int index = line.indexOf("25:");
					nosaccount = line.substring(index + 3);
					/*
					 * if(transactionrefNumber != null) { payMentDetails = transactionrefNumber; }
					 */
				}
			}
			br1.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return nosaccount;
	}

	public static String createOutputDirectory(String suiteLoc, String directoryName) {
		File file = new File(suiteLoc + "/" + directoryName);
		if (!file.exists()) {
			if (file.mkdir()) {
				System.out.println("Directory is created!");
			} else {
				System.out.println("Failed to create directory!");
			}
		}
		return file.getAbsolutePath();
	}
	
	
	//-------------------------------------------Deepak-----------------------------------------------------------------------------------
	
	
	 public static void updateFedAckFile2(String sourcefilePath, String destfilePath, String outputInstructionId)
     {
         try{
             System.out.println("sourcefilePath statement::::::::::"+sourcefilePath);
             System.out.println("destfilePath statement::::::::::"+destfilePath);
             String currentLine="";
             String oldContent="";
             String modifiedValueData = "";
             File file =new File(sourcefilePath);
             FileReader fr =new FileReader(file);
             BufferedReader br =new BufferedReader(fr);


             while((currentLine=br.readLine())!=null)
             {
                 oldContent=oldContent+currentLine+System.lineSeparator();
             }
             System.out.println("oldContent::::::::"+oldContent);
             if(outputInstructionId != null)
             {
                 System.out.println("paydetails[2]::::::::::::::::::::::::"+outputInstructionId);
                 modifiedValueData = oldContent.replace("DATELTERMIDINSTRUC", outputInstructionId);
                 System.out.println("modifiedValueData1::::::::::::::::::::::::::::::::::::::::::::"+modifiedValueData);
                 Calendar calendar = Calendar.getInstance();
                  SimpleDateFormat dateFormat = new SimpleDateFormat("MMdd");
                  System.out.println(dateFormat.format(calendar.getTime()));
                  modifiedValueData = modifiedValueData.replace("LAST", dateFormat.format(calendar.getTime()));
                 System.out.println("modifiedValueData2::::::::::::::::::::::::::::::::::::::::::::"+modifiedValueData);
                 modifiedValueData = modifiedValueData.replace("Last24Spaces------------", "                        ");
             }
//             String finaldata = modifiedValueData.trim();
             String finaldata = modifiedValueData;
             FileWriter fw =new FileWriter(destfilePath);
             br.close();
             fw.write(finaldata, 0, 125);
             fw.flush();
             fw.close();
         }
         catch(Exception e)
         {
             e.printStackTrace();
         }
     }
	 
	 
	 public static void updateFedAckFileForBackDated(String sourcefilePath, String destfilePath, String outputInstructionId) {
	        try {
	            System.out.println("sourcefilePath statement::::::::::" + sourcefilePath);
	            System.out.println("destfilePath statement::::::::::" + destfilePath);
	            String currentLine = "";
	            String oldContent = "";
	            String modifiedValueData = "";
	            File file = new File(sourcefilePath);
	            FileReader fr = new FileReader(file);
	            BufferedReader br = new BufferedReader(fr);

	 

	            while ((currentLine = br.readLine()) != null) {
	                oldContent = oldContent + currentLine + System.lineSeparator();
	            }
	            System.out.println("oldContent::::::::" + oldContent);
	            if (outputInstructionId != null) {
	                System.out.println("paydetails[2]::::::::::::::::::::::::" + outputInstructionId);
	                modifiedValueData = oldContent.replace("DATELTERMIDINSTRUC", outputInstructionId);
	                System.out
	                        .println("modifiedValueData1::::::::::::::::::::::::::::::::::::::::::::" + modifiedValueData);

	 

	                String sDate = Utils.getSubString(outputInstructionId, 0, 4);
	                System.out.println(sDate);
	                modifiedValueData = modifiedValueData.replace("LAST", sDate);
	                System.out
	                        .println("modifiedValueData2::::::::::::::::::::::::::::::::::::::::::::" + modifiedValueData);
	                Random rand = new Random();
	                int value = rand.nextInt(200);
	                String sValue = Integer.toString(value);
	                System.out.println(sValue);
	                modifiedValueData = modifiedValueData.replace("ran", sValue);
	                System.out
	                        .println("modifiedValueData2::::::::::::::::::::::::::::::::::::::::::::" + modifiedValueData);

	 

	            }
	            String finaldata = modifiedValueData;
	            int iFileLength=finaldata.length()-2;
	            FileWriter fw = new FileWriter(destfilePath);
	            br.close();
	            fw.write(finaldata, 0, iFileLength);
	            fw.flush();
	            fw.close();

	 

	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	 
	 
	 
	 public static void updateFedNackFile1(String sourcefilePath, String destfilePath, String outputInstructionId) {
			try {
				System.out.println("sourcefilePath statement::::::::::" + sourcefilePath);
				System.out.println("destfilePath statement::::::::::" + destfilePath);
				String currentLine = "";
				String oldContent = "";
				String modifiedValueData = "";
				File file = new File(sourcefilePath);
				FileReader fr = new FileReader(file);
				BufferedReader br = new BufferedReader(fr);

				while ((currentLine = br.readLine()) != null) {
					oldContent = oldContent + currentLine + System.lineSeparator();
				}
				System.out.println("oldContent::::::::" + oldContent);
				if (outputInstructionId != null) {
					System.out.println("paydetails[2]::::::::::::::::::::::::" + outputInstructionId);
					modifiedValueData = oldContent.replace("OUTPUT_INSTRUCTIONN_ID", outputInstructionId);
					System.out
							.println("modifiedValueData1::::::::::::::::::::::::::::::::::::::::::::" + modifiedValueData);

				}
				String finaldata = modifiedValueData;
				int iFileLength=finaldata.length()-2;
				FileWriter fw = new FileWriter(destfilePath);
				br.close();
				fw.write(finaldata,0,iFileLength);
				fw.flush();
				fw.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
     
	 
     //-----------------------------------------------End------------------------------------------------------------------------------------------
     
	 
	 
	 

	
	public static void incomingReturnSVCFileUpdate(String sourcefilePath, String destfilePath, String outputInstructionId)
    {
        try{
            System.out.println("sourcefilePath statement::::::::::"+sourcefilePath);
            System.out.println("destfilePath statement::::::::::"+destfilePath);
            String currentLine="";
            String oldContent="";
            String modifiedValueData = "";
            File file =new File(sourcefilePath);
            FileReader fr =new FileReader(file);
            BufferedReader br =new BufferedReader(fr);

 

            while((currentLine=br.readLine())!=null)
            {
                oldContent=oldContent+currentLine+System.lineSeparator();
            }
            System.out.println("oldContent::::::::"+oldContent);
            if(outputInstructionId != null)
            {
                System.out.println("paydetails[2]::::::::::::::::::::::::"+outputInstructionId);
                modifiedValueData = oldContent.replace("OUTPUT_INSTRUCTIONN_ID", outputInstructionId);
                System.out.println("modifiedValueData1::::::::::::::::::::::::::::::::::::::::::::"+modifiedValueData);
                
        		String ltrm = outputInstructionId.substring(0, 16);
        		String lst = outputInstructionId.substring(16, 22);
        		int num = Integer.parseInt(lst);
        		int addednum = num+3;
        		String formattedStr = String.format("%06d", addednum);
        		String newInstrToUpdate = ltrm+formattedStr;
        		System.out.println("newInstr "+newInstrToUpdate);
                modifiedValueData = modifiedValueData.replace("OUTPUT_INSTRUCTION_IDN", newInstrToUpdate);
                System.out.println("modifiedValueData1::::::::::::::::::::::::::::::::::::::::::::"+modifiedValueData);
        		
            }
            String finaldata = modifiedValueData;
            int len = finaldata.length();
            FileWriter fw =new FileWriter(destfilePath);
            br.close();
            fw.write(finaldata, 0, len-2);
            fw.flush();
            fw.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
	
	public static String incomingReturnSVCFileUpdate_Unmatched(String sourcefilePath, String destfilePath, String outputInstructionId)
    {
		String newInstrToUpdate = null;
        try{
            System.out.println("sourcefilePath statement::::::::::"+sourcefilePath);
            System.out.println("destfilePath statement::::::::::"+destfilePath);
            String currentLine="";
            String oldContent="";
            String modifiedValueData = "";
            File file =new File(sourcefilePath);
            FileReader fr =new FileReader(file);
            BufferedReader br =new BufferedReader(fr);

 

            while((currentLine=br.readLine())!=null)
            {
                oldContent=oldContent+currentLine+System.lineSeparator();
            }
            System.out.println("oldContent::::::::"+oldContent);
            if(outputInstructionId != null)
            {
           		String ltrm1 = outputInstructionId.substring(0, 16);
        		String lst1 = outputInstructionId.substring(16, 22);
        		int num1 = Integer.parseInt(lst1);
        		int addednum1 = num1+2;
        		String formattedStr1 = String.format("%06d", addednum1);
        		String newInstrToUpdate1 = ltrm1+formattedStr1;
        		System.out.println("newInstr "+newInstrToUpdate1);
            	
                System.out.println("paydetails[2]::::::::::::::::::::::::"+outputInstructionId);
                modifiedValueData = oldContent.replace("OUTPUT_INSTRUCTIONN_ID", newInstrToUpdate1);
                System.out.println("modifiedValueData1::::::::::::::::::::::::::::::::::::::::::::"+modifiedValueData);
                
        		String ltrm = outputInstructionId.substring(0, 16);
        		String lst = outputInstructionId.substring(16, 22);
        		int num = Integer.parseInt(lst);
        		int addednum = num+1;
        		String formattedStr = String.format("%06d", addednum);
        		newInstrToUpdate = ltrm+formattedStr;
        		System.out.println("newInstr "+newInstrToUpdate);
                modifiedValueData = modifiedValueData.replace("OUTPUT_INSTRUCTION_IDN", newInstrToUpdate);
                System.out.println("modifiedValueData1::::::::::::::::::::::::::::::::::::::::::::"+modifiedValueData);
        		
            }
            String finaldata = modifiedValueData;
            int len = finaldata.length();
            FileWriter fw =new FileWriter(destfilePath);
            br.close();
            fw.write(finaldata, 0, len-2);
            fw.flush();
            fw.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
		return newInstrToUpdate;
    }
	
	public static void incomingReturnSVCFile_PreviousDaateUpdate(String sourcefilePath, String destfilePath, String outputInstructionId)
    {
        try{
            System.out.println("sourcefilePath statement::::::::::"+sourcefilePath);
            System.out.println("destfilePath statement::::::::::"+destfilePath);
            String currentLine="";
            String oldContent="";
            String modifiedValueData = "";
            File file =new File(sourcefilePath);
            FileReader fr =new FileReader(file);
            BufferedReader br =new BufferedReader(fr);

 

            while((currentLine=br.readLine())!=null)
            {
                oldContent=oldContent+currentLine+System.lineSeparator();
            }
            System.out.println("oldContent::::::::"+oldContent);
            if(outputInstructionId != null)
            {
                System.out.println("paydetails[2]::::::::::::::::::::::::"+outputInstructionId);
                modifiedValueData = oldContent.replace("OUTPUT_INSTRUCTIONN_ID", outputInstructionId);
                System.out.println("modifiedValueData1::::::::::::::::::::::::::::::::::::::::::::"+modifiedValueData);
                
        		String ltrm = outputInstructionId.substring(0, 16);
        		//String ltrmYr = ltrm.substring(0,4);
        		String lst = outputInstructionId.substring(16, 22);
        		int num = Integer.parseInt(lst);
        		int addednum = num+1;
        		String formattedStr = String.format("%06d", addednum);
        		//System.out.println("formattedStr "+formattedStr);
				Calendar calendar = Calendar.getInstance();
				 SimpleDateFormat dateFormat = new SimpleDateFormat("MMdd");				 
				 //System.out.println(dateFormat.format(calendar.getTime()));
        		String newInstrToUpdate = ltrm+formattedStr;
        		System.out.println("newInstr : "+newInstrToUpdate);
        		String[] splt = newInstrToUpdate.split("LTERMID");
        		String dtYr = splt[0].substring(0,4);
        		System.out.println("New Date : "+dtYr+dateFormat.format(calendar.getTime())+"LTERMID"+splt[1]);
        		String newDateToReoplace = dtYr+dateFormat.format(calendar.getTime())+"LTERMID"+splt[1];
                modifiedValueData = modifiedValueData.replace("OUTPUT_INSTRUCTION_IDN", newDateToReoplace);
                System.out.println("modifiedValueData1::::::::::::::::::::::::::::::::::::::::::::"+modifiedValueData);
        		
            }
            String finaldata = modifiedValueData;
            int len = finaldata.length();
            FileWriter fw =new FileWriter(destfilePath);
            br.close();
            fw.write(finaldata, 0, len-2);
            fw.flush();
            fw.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
